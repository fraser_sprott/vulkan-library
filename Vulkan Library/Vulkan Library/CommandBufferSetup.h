#ifndef COMMANDBUFFERSETUP_H
#define COMMANDBUFFERSETUP_H

#include "VKLib.h"
#include "DeviceSetup.h"

#include <vector>

using namespace std;

class CommandBufferSetup
{
public:
	CommandBufferSetup() {}
	CommandBufferSetup(DeviceSetup* device);
	~CommandBufferSetup();
	void createCommandBuffers();
	void createFrameBuffer();
	void createCommandPool();
	void beginCommandBufferRecording(int commandBuffer);
	void endCommandBufferRecording(int commandBuffer);
	VkCommandBufferBeginInfo getBasicCommandBufferBeginInfo();

	//getters
	vector<VkFramebuffer> getFrameBuffers(){ return this->frameBuffer; }
	vector<VkCommandBuffer> getCommandBuffers() { return this->commandBuffer; }
	VkCommandBuffer getCommandBuffer(int no) { return this->commandBuffer[no]; }
	VkCommandPool getCommandPool() { return this->commandPool; }
private:
	DeviceSetup* device;

	vector<VkFramebuffer> frameBuffer;
	vector<VkCommandBuffer> commandBuffer;
	VkCommandPool commandPool;
};

#endif
