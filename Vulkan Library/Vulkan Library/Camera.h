#ifndef CAMERA_H
#define CAMERA_H

#include "VKLib.h"

#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include "glm.hpp"
#include "gtc\matrix_transform.hpp"

#define DEG_TO_RADIANS 0.017453293

class Camera
{
public:
	Camera();
	Camera(glm::vec3 eye, glm::vec3 at, glm::vec3 pos, float rotation);
	~Camera();
	void moveForward(float amount);
	void moveRight(float amount);
	void moveUp(float amount);
	void rotate(float amount);
	void updateMatrix();
	//getters
	glm::mat4 getCameraMatrix() { return cameraMatrix; }
	glm::vec3 getPos() { return eye; }
private:
	glm::vec3 eye; //pos
	glm::vec3 at; //look at
	glm::vec3 up; //up direction
	glm::mat4 cameraMatrix; //cameraMatrix
	float r;
};

#endif
