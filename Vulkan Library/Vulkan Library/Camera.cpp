#include "Camera.h"



Camera::Camera()
{
	eye = glm::vec3(-1.0, -1.0, -1.0);
	at = glm::vec3(0.0f, 0.0f, 0.0f);
	up = glm::vec3(0.0f, -1.0f, 0.0f);
	r = 0.0;
	updateMatrix();
}

Camera::Camera(glm::vec3 eye, glm::vec3 at, glm::vec3 up, float rotation)
{
	this->eye = eye;
	this->at = at;
	this->up = up;
	this->r = rotation;
	updateMatrix();
}

Camera::~Camera()
{

}

void Camera::moveForward(float amount)
{
	eye = glm::vec3(eye.x + amount*std::sin(r*DEG_TO_RADIANS), eye.y, eye.z - amount*std::cos(r*DEG_TO_RADIANS));
	updateMatrix();
}

void Camera::moveRight(float amount)
{
	eye = glm::vec3(eye.x + amount*std::cos(r*DEG_TO_RADIANS), eye.y, eye.z + amount*std::sin(r*DEG_TO_RADIANS));
	updateMatrix();
}

void Camera::moveUp(float amount)
{
	eye.y += amount;
	updateMatrix();
}

//change the rotation
void Camera::rotate(float amount)
{
	r += amount;
	updateMatrix();
}

void Camera::updateMatrix()
{
	cameraMatrix = glm::lookAt(eye, at, up);
}