#include "DeviceSetup.h"

DeviceSetup::DeviceSetup()
{

}

DeviceSetup::DeviceSetup(int screenWidth, int screenHeight)
{
	//get the extensions
	getAvailableWSIExtensions();	

	//if debug include layers
#if defined(_DEBUG)
	layers.push_back("VK_LAYER_LUNARG_standard_validation");
	layers.push_back("VK_LAYER_LUNARG_core_validation");
#endif
	
	this->screenWidth = screenWidth;
	this->screenHeight = screenHeight;

	//create the window
	setupRC();
	//initialise vulkan
	initialiseVulkan();
	//create the vulkan surface
	createVulkanSurface();
	//get the physical device
	createPhysicalDevice();
	//get the logic device
	createLogicalDevice();
	//create the swapChain
	createSwapChain();
	//create the image view
	createImageView();
	//create the render pass
	createRenderPass();
}

DeviceSetup::~DeviceSetup()
{
	//clean up
	vkDestroyDevice(this->logicDevice, VK_NULL_HANDLE);
	vkDestroySurfaceKHR(this->instance, this->surface, VK_NULL_HANDLE);
	SDL_DestroyWindow(this->window);
	SDL_Quit();
	vkDestroyInstance(this->instance, VK_NULL_HANDLE);
}

//gets the availabele wsi extensions
void DeviceSetup::getAvailableWSIExtensions()
{
	this->extensions;
	this->extensions.push_back(VK_KHR_SURFACE_EXTENSION_NAME);

	//use if running on a windows device
#if defined(VK_USE_PLATFORM_WIN32_KHR)
	this->extensions.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
#endif

}

void DeviceSetup::setupRC() {
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {// Initialize video
		VKLib::exitFatalError("Failed to initilise SDL");
	}

	//Create a sdl window
	this->window = SDL_CreateWindow("VKLib Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, this->screenWidth, this->screenHeight, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
}

void DeviceSetup::initialiseVulkan()
{
	// VkApplicationInfo allows the programmer to specifiy some basic information about the
	// program, which can be useful for layers and tools to provide more debug information.
	VkApplicationInfo appInfo;
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pNext = VK_NULL_HANDLE;
	appInfo.pApplicationName = "VKLib Demo";
	appInfo.applicationVersion = 1;
	appInfo.pEngineName = "VKLib";
	appInfo.engineVersion = 1;
	appInfo.apiVersion = VK_API_VERSION_1_0;

	// VkInstanceCreateInfo is where the programmer specifies the layers and/or extensions that
	// are needed.
	VkInstanceCreateInfo instInfo;
	instInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instInfo.pNext = VK_NULL_HANDLE;
	instInfo.flags = 0;
	instInfo.pApplicationInfo = &appInfo;
	instInfo.enabledExtensionCount = static_cast<uint32_t>(this->extensions.size());
	instInfo.ppEnabledExtensionNames = this->extensions.data();
	instInfo.enabledLayerCount = static_cast<uint32_t>(this->layers.size());
	instInfo.ppEnabledLayerNames = this->layers.data();
	instInfo.flags = 0;
	instInfo.pNext = VK_NULL_HANDLE;

	// Create the Vulkan instance.
	VkResult result = vkCreateInstance(&instInfo, VK_NULL_HANDLE, &this->instance);

	//handle all potential errors 
	switch (result)
	{
	case VK_SUCCESS:
		cout << "Vulkan Instance Created" << endl;
		break;
	case VK_ERROR_OUT_OF_HOST_MEMORY:
		VKLib::exitFatalError("Can't create a Vulkan Instace. Out of Host Memory ");
		break;
	case VK_ERROR_OUT_OF_DEVICE_MEMORY:
		VKLib::exitFatalError("Can't create a Vulkan Instace. Out of Device Memory ");
		break;
	case VK_ERROR_INITIALIZATION_FAILED:
		VKLib::exitFatalError("Can't create a Vulkan Instace. Initialization Failed ");
		break;
	case VK_ERROR_LAYER_NOT_PRESENT:
		VKLib::exitFatalError("Can't create a Vulkan Instace. Layer Not Present");
		break;
	case VK_ERROR_EXTENSION_NOT_PRESENT:
		VKLib::exitFatalError("Can't create a Vulkan Instace. Extension Not Present");
		break;
	case VK_ERROR_INCOMPATIBLE_DRIVER:
		VKLib::exitFatalError("Can't create a Vulkan Instace. Incompatable Driver");
		break;
	default:
		VKLib::exitFatalError("Can't create a Vulkan Instace. Unknown Error");
		break;
	}
}

void DeviceSetup::createVulkanSurface()
{
	//get the window info
	SDL_SysWMinfo windowInfo;
	SDL_VERSION(&windowInfo.version);
	if (!SDL_GetWindowWMInfo(window, &windowInfo)) {
		VKLib::exitFatalError("SDK window manager info is not available.");
	}

	switch (windowInfo.subsystem) {

	//check to make sure that windows is being used and set the surface info
#if defined(SDL_VIDEO_DRIVER_WINDOWS) && defined(VK_USE_PLATFORM_WIN32_KHR)
	case SDL_SYSWM_WINDOWS: {
		VkWin32SurfaceCreateInfoKHR surfaceInfo;
		surfaceInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR; //type
		surfaceInfo.hinstance = GetModuleHandle(VK_NULL_HANDLE); //get
		surfaceInfo.hwnd = windowInfo.info.win.window;

		//not used but still needs to be set up
		surfaceInfo.flags = 0;
		surfaceInfo.pNext = VK_NULL_HANDLE;

		//create the info and check the result
		VkResult result = vkCreateWin32SurfaceKHR(this->instance, &surfaceInfo, VK_NULL_HANDLE, &this->surface);
		if (result != VK_SUCCESS) {
			VKLib::exitFatalError("Failed to create Win32 surface."); //display error
		}
		break;
	}
#endif
	default:
		VKLib::exitFatalError("Unsuported window Manager");
	}
	//if got here creation worked
}

void DeviceSetup::createPhysicalDevice()
{
	uint32_t noOfDevices = 0;
	//get how many devices are available
	vkEnumeratePhysicalDevices(this->instance, &noOfDevices, nullptr);

	//if there is no devices availble
	if (noOfDevices == 0) {
		VKLib::exitFatalError("No suitable device available");
	}

	//get all the devices and store them 
	vector<VkPhysicalDevice> availableDevices(noOfDevices);
	vkEnumeratePhysicalDevices(this->instance, &noOfDevices, availableDevices.data());

	bool deviceChosen = false;
	//check the suitability of the devices
	for (int i = 0; i < noOfDevices; i++)
	{
		if (checkDeviceSuitability(availableDevices[i]))
		{
			//this device is suitable
			this->physicalDevice = availableDevices[i];
			deviceChosen = true;
			break;
		}
	}

	//if no device was chosen exit
	if (deviceChosen == false)
	{
		VKLib::exitFatalError("No suitable device available");
	}
}

bool DeviceSetup::checkDeviceSuitability(VkPhysicalDevice device)
{
	VkPhysicalDeviceProperties deviceProperties;
	VkPhysicalDeviceFeatures deviceFeatures;
	vkGetPhysicalDeviceProperties(device, &deviceProperties);
	vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

	//get the queue families suported by this device/check is done inside the function
	VKLib::graphicFamilyindex index = VKLib::getgraphicFamilies(device, this->surface);

	bool suitable = true; //turn false if device doesnt support a feature

						  //check to see if the device type is descrete
	if (deviceProperties.deviceType != VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
		suitable = false;

	//check the queue support 
	if (index.graphicFam < 0)
		suitable = false;

	//check the extensions
	bool swapChainAdequate = false;
	//if the extensions are empty dont check
	if (extensions.size() > 0) {
		//query the swapchain support
		VKLib::SwapChainSupport swapChainSupport = VKLib::querySwapChainSupport(device, this->surface);
		//suitable is true only if surfaceFormats and presentModes is not empty
		suitable = !swapChainSupport.surfaceFormats.empty() && !swapChainSupport.presentModes.empty();
	}

	return suitable;
}

void DeviceSetup::createLogicalDevice() {
	//get the supported queues
	VKLib::graphicFamilyindex index = VKLib::getgraphicFamilies(this->physicalDevice, this->surface);
	vector<VkDeviceQueueCreateInfo> queueCreateInfo;
	set<int> uniqueQueueFamilies = { index.graphicFam, index.presentFam };

	//always needs to be declared, even when 1 queue
	float queuePriority = 1.0f;
	for (int queueFamily : uniqueQueueFamilies) {
		VkDeviceQueueCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		createInfo.queueFamilyIndex = queueFamily;
		createInfo.queueCount = 1;
		createInfo.pQueuePriorities = &queuePriority;
		createInfo.pNext = VK_NULL_HANDLE; //not used
		createInfo.flags = 0; //not used
		queueCreateInfo.push_back(createInfo);
	}

	//device specific info used for creation
	VkPhysicalDeviceFeatures deviceFeatures = {}; //no features required yet
	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO; //structure type
	createInfo.pQueueCreateInfos = queueCreateInfo.data(); //queueCreaterInfo vector
	createInfo.queueCreateInfoCount = (uint32_t)queueCreateInfo.size(); // number of queues
	createInfo.pEnabledFeatures = &deviceFeatures; //device features
	createInfo.enabledExtensionCount = this->deviceExtensions.size();//enabled extensions
	createInfo.ppEnabledExtensionNames = this->deviceExtensions.data(); //device extensions
																  //not used but needs to be set up
	createInfo.flags = 0;
	createInfo.pNext = VK_NULL_HANDLE;

	//only use validation layers in debug
	if (_DEBUG) {
		createInfo.enabledLayerCount = this->layers.size();
		createInfo.ppEnabledLayerNames = this->layers.data();
	}

	//create the logic device
	if (vkCreateDevice(this->physicalDevice, &createInfo, nullptr, &this->logicDevice) != VK_SUCCESS) {
		VKLib::exitFatalError("Could not create logic device");
	}

	//last thing needed is to get both queue handles from the logic device
	vkGetDeviceQueue(this->logicDevice, index.graphicFam, 0, &queue);
	vkGetDeviceQueue(this->logicDevice, index.presentFam, 0, &presentQueue);
}

//creates the swap chain
void DeviceSetup::createSwapChain() {
	//decide what format to use for the surface
	VKLib::SwapChainSupport support = querySwapChainSupport(this->physicalDevice, this->surface);

	//get the various components of the swap chain 
	VkSurfaceFormatKHR surfaceFormat = SwapChainSurfaceFormat(support.surfaceFormats);
	VkPresentModeKHR presentMode = SwapChainPresentMode(support.presentModes);
	this->chainExtent = chooseSwapChainExtent(support.surfaceCapabilities, this->screenWidth, this->screenHeight);//chainExtent is needed for the imageView creation
	uint32_t imageCount = swapChainImageCount(support);

	//set the information needed to create the swap chain
	VkSwapchainCreateInfoKHR createInfo;
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = this->surface; //set thesurface to it
	createInfo.minImageCount = imageCount; // the maximum size of the queue
	createInfo.imageFormat = surfaceFormat.format; //the surface format
	createInfo.imageColorSpace = surfaceFormat.colorSpace;
	createInfo.imageExtent = this->chainExtent; //resolution
	createInfo.imageArrayLayers = 1; //the amount of layers for each image always 1 unless 3D stereoscopic app 
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT; //render directly to the image
	createInfo.flags = 0;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

	//retrieve the index
	VKLib::graphicFamilyindex index = VKLib::getgraphicFamilies(physicalDevice, surface);

	uint32_t graphicFamilyIndices[] = { (uint32_t)index.graphicFam, (uint32_t)index.presentFam };

	//check to see if the queues are equal and if so set to sharing mode
	if (index.graphicFam != index.presentFam) {
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT; // no queue has ownership over an image
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = graphicFamilyIndices;
	}
	else {
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE; //queue has ownership of an image and needs to be specigficaly transferred to another
		createInfo.queueFamilyIndexCount = 0; // Optional
		createInfo.pQueueFamilyIndices = nullptr; // Optional
	}

	//final creation info
	createInfo.preTransform = support.surfaceCapabilities.currentTransform;
	createInfo.presentMode = presentMode;
	createInfo.clipped = VK_TRUE;
	createInfo.oldSwapchain = VK_NULL_HANDLE;

	createInfo.pNext = VK_NULL_HANDLE;
	createInfo.flags = 0;

	if (vkCreateSwapchainKHR(this->logicDevice, &createInfo, nullptr, &this->swapChain) != VK_SUCCESS) {
		VKLib::exitFatalError("Could not create swap chain");
	}

	//get the handle to the swap chain Images
	vkGetSwapchainImagesKHR(this->logicDevice, this->swapChain, &imageCount, nullptr);
	chainImages.resize(imageCount);
	vkGetSwapchainImagesKHR(this->logicDevice, this->swapChain, &imageCount, chainImages.data());

	//store this as it will be needed to create the image view
	this->imageFormat = surfaceFormat.format;
}

void DeviceSetup::createImageView()
{
	//resize the list
	this->imageViews.resize(this->chainImages.size());

	//loop for each image
	for (uint32_t i = 0; i < this->chainImages.size(); i++) {
		VkImageViewCreateInfo createInfo;
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO; //tyep of createInfo
		createInfo.image = this->chainImages[i];//pass the current image to it
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D; //interperate the image as 2d 
		createInfo.format = this->imageFormat;
		createInfo.flags = 0;
		createInfo.pNext = VK_NULL_HANDLE;
		//stick to the default mapping
		createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

		//sets the images purpose and describes how it shoudl be accessed
		createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		createInfo.subresourceRange.baseMipLevel = 0;
		createInfo.subresourceRange.levelCount = 1;
		createInfo.subresourceRange.baseArrayLayer = 0;
		createInfo.subresourceRange.layerCount = 1;

		//create the image view
		if (vkCreateImageView(this->logicDevice, &createInfo, nullptr, &this->imageViews[i]) != VK_SUCCESS) {
			VKLib::exitFatalError("could not create image view");
		}
	}
}

//creates the render pass
void DeviceSetup::createRenderPass()
{
	VkAttachmentDescription colorAttch;
	colorAttch.format = this->imageFormat;//match the swapchain image
	colorAttch.samples = VK_SAMPLE_COUNT_1_BIT;//multisampling basic implementiation wont use it, so set to one sample
	colorAttch.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR; //what to do with data before and after rendering,
	colorAttch.storeOp = VK_ATTACHMENT_STORE_OP_STORE; //rendered constants will be stored in memory
	colorAttch.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE; //basic implementation wont use stencils. 
	colorAttch.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE; //VK_ATTACHMENT_STORE_OP_DONT_CARE means contents of framebuffers are undefined after rendering
	colorAttch.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED; //the initial layout of the image before the renderpass starts
	colorAttch.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR; //the layout of the image to go to after the render pass is done
	colorAttch.flags = 0;

	VkAttachmentReference colorAttchRef;
	colorAttchRef.attachment = 0; //specifies what attachment to reference in an array
	colorAttchRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL; //the layout to attach to the subpass 

	VkSubpassDescription subPass = {};
	subPass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS; //specify that this is for graphics use
	subPass.colorAttachmentCount = 1; //use only one color atachment 
	subPass.pColorAttachments = &colorAttchRef; //set colorAttchRef to color attachment

	//set up the subpass dependency
	VkSubpassDependency dependency;
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.srcAccessMask = 0;
	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependency.dependencyFlags = 0;

	//create render pass 
	VkRenderPassCreateInfo passInfo;
	passInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO; //structure type 
	passInfo.attachmentCount = 1; //set 1 attachment
	passInfo.pAttachments = &colorAttch; //attach color attach
	passInfo.subpassCount = 1; //set 1 subpass
	passInfo.pSubpasses = &subPass; //pass sub pass in
	passInfo.dependencyCount = 1;
	passInfo.pDependencies = &dependency;
	passInfo.flags = 0;
	passInfo.pNext = VK_NULL_HANDLE;

	//create the render pass
	if (vkCreateRenderPass(this->logicDevice, &passInfo, nullptr, &this->renderPass) != VK_SUCCESS)
	{
		cout << "Could not create the render pass";
	}
}

//chooses a format supported
VkSurfaceFormatKHR DeviceSetup::SwapChainSurfaceFormat(std::vector<VkSurfaceFormatKHR> supportedFormats)
{
	for (int i = 0; i < supportedFormats.size(); i++)
	{
		//VK_FORMAT_B8G8R8A8_UNORM means that there is 8bits per colour channel i.e 8 bits for red, green, blue and alpha
		//VK_COLOR_SPACE_SRGB_NONLINEAR_KHR allows for more arcurate colours
		if (supportedFormats[i].format == VK_FORMAT_B8G8R8A8_UNORM && supportedFormats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			return supportedFormats[i];
		}
	}
	//IF none use the formatting use the first one
	return supportedFormats[0];
}

//gets the swap present mode//retrives the best one available
VkPresentModeKHR DeviceSetup::SwapChainPresentMode(vector<VkPresentModeKHR> availablePresentModes)
{
	//loop through each of the available present modes
	for (int i = 0; i < availablePresentModes.size(); i++)
	{
		//the mailbox is similar to the fifo one but works the best and when queue is full it will replace objectsinstead of waiting
		if (availablePresentModes[i] == VK_PRESENT_MODE_MAILBOX_KHR) {
			return availablePresentModes[i];
		}
	}

	//Fifo is the next best presentmode. should always be available
	return VK_PRESENT_MODE_FIFO_KHR;
}

//defines the resolution of the swap chains images
VkExtent2D DeviceSetup::chooseSwapChainExtent(const VkSurfaceCapabilitiesKHR& capabilities, int screenWidth, int screenHeight)
{
	//if the current extent width does not match the maximum 
	if (capabilities.currentExtent.width != numeric_limits<uint32_t>::max()) {
		return capabilities.currentExtent;
	}

	//set actual extent to the screen dimensions
	VkExtent2D actualExtent = { screenWidth, screenHeight };

	//for each dimension set the dimension to the maximum of the screen and the imageExtent. this allows for the closest match
	actualExtent.width = max(capabilities.minImageExtent.width, min(capabilities.maxImageExtent.width, actualExtent.width));
	actualExtent.height = max(capabilities.minImageExtent.height, min(capabilities.maxImageExtent.height, actualExtent.height));

	return actualExtent;
}

//the number of the 
uint32_t DeviceSetup::swapChainImageCount(VKLib::SwapChainSupport support)
{
	//if imageCount is zero its only restricted by memory 
	uint32_t imageCount = support.surfaceCapabilities.minImageCount + 1;

	//if maxImagecount is greater than zero and image count is greater than the maximum. Set the imageCountto the maximum supported
	if (support.surfaceCapabilities.maxImageCount > 0 && imageCount > support.surfaceCapabilities.maxImageCount) {
		imageCount = support.surfaceCapabilities.maxImageCount;
	}

	return imageCount;
}

VKLib::SwapChainSupport DeviceSetup::querySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface) {
	//create a handle to get the swap chain support
	VKLib::SwapChainSupport details;

	//get the device surface capabilities
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.surfaceCapabilities);

	//get the surface formats
	uint32_t formatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);

	if (formatCount != 0) {
		details.surfaceFormats.resize(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.surfaceFormats.data());
	}

	//same for the presentMode
	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

	if (presentModeCount != 0) {
		details.presentModes.resize(presentModeCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.presentModes.data());
	}

	return details;
}