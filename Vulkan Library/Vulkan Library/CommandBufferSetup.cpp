#include "CommandBufferSetup.h"



CommandBufferSetup::CommandBufferSetup(DeviceSetup* device)
{
	this->device = device;
	//createthe frame buffer
	createFrameBuffer();
	//create the commandPool
	createCommandPool();
	//create the commandBuffer
	createCommandBuffers();
}


CommandBufferSetup::~CommandBufferSetup()
{

}

void CommandBufferSetup::createCommandBuffers()
{
	//make the command buffer size match the frame buffer
	this->commandBuffer.resize(this->frameBuffer.size());

	//used to check if creation was successful
	VkResult result;

	//create a default one
	VkCommandBufferAllocateInfo allocateInfo;
	allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO; //struct type
	allocateInfo.commandPool = this->commandPool; //the command pool
	allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY; //this buffer cant be called by another buffer
	allocateInfo.commandBufferCount = (uint32_t)this->commandBuffer.size(); //the number of command buffers
	allocateInfo.pNext = VK_NULL_HANDLE;
	//create the commandBuffers
	result = vkAllocateCommandBuffers(this->device->getLogicDevice(), &allocateInfo, this->commandBuffer.data());

	//check the result
	switch (result) {
	case VK_SUCCESS:
		//buffer created do nothing
		break;
	case VK_ERROR_OUT_OF_HOST_MEMORY:
		//failed, display message and return nullptr
		VKLib::exitFatalError("command creation failed. Out of host memory");
		break;
	case VK_ERROR_OUT_OF_DEVICE_MEMORY:
		//failed, display message and return nullptr
		VKLib::exitFatalError("command creation failed. Out of device memory");
		break;
	default:
		//failed, display message and return nullptr
		VKLib::exitFatalError("command creation failed. Unkown reason");
		break;
	}
}

//creates the frame buffer
//device - logical device that the frame buffer will be created on
//renderPass - a created render pass
//chain extent - a valid chain extent
//imageView - the VkImageView vector that will be used for the buffer
void CommandBufferSetup::createFrameBuffer()
{
	//set the size to the match the number of imageviews
	//this means there is a frame buffer for every image view
	vector<VkImageView> imageView = device->getImageViews();
	this->frameBuffer.resize(imageView.size());

	for (int i = 0; i < imageView.size(); i++) {
		//store the buffer create info
		VkFramebufferCreateInfo bufferInfo;
		bufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO; //structure type
		bufferInfo.renderPass = this->device->getRenderPass(); //render pass
		bufferInfo.attachmentCount = 1; //number of image view adding
		bufferInfo.pAttachments = &imageView[i];
		bufferInfo.width = this->device->getChainExtent().width; //buffer width - match the screen width
		bufferInfo.height = this->device->getChainExtent().height; // buffer height - match the screen height
		bufferInfo.layers = 1; //number of layers
		bufferInfo.flags = 0; //set flag to 0
		bufferInfo.pNext = VK_NULL_HANDLE;
		//create the frameBuffer
		VkResult result = vkCreateFramebuffer(this->device->getLogicDevice(), &bufferInfo, nullptr, &frameBuffer[i]); 

		//check result
		switch (result) {
		case VK_SUCCESS:
			//buffer created do nothing
			break;
		case VK_ERROR_OUT_OF_HOST_MEMORY:
			//failed, display message and return nullptr
			VKLib::exitFatalError("buffer creation failed. Out of host memory");
			break;
		case VK_ERROR_OUT_OF_DEVICE_MEMORY:
			//failed, display message and return nullptr
			VKLib::exitFatalError("buffer creation failed. Out of device memory");
			break;
		default:
			//failed, display message and return nullptr
			VKLib::exitFatalError("buffer creation failed. Unkown reason");
			break;
		}
	}
}

//creates the command pool
void CommandBufferSetup::createCommandPool()
{
	//get the device queues
	VKLib::graphicFamilyindex index = VKLib::getgraphicFamilies(this->device->getPhysicalDevice(), this->device->getSurface());

	//create the command pool info
	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO; //struct type
	poolInfo.queueFamilyIndex = index.graphicFam; //add the queue index
	poolInfo.flags = 0; //add the buffer flag
	poolInfo.pNext = VK_NULL_HANDLE;
	//create the command pool
	VkResult result = vkCreateCommandPool(this->device->getLogicDevice(), &poolInfo, nullptr, &this->commandPool);

	//check result
	switch (result) {
	case VK_SUCCESS:
		//buffer created do nothing
		break;
	case VK_ERROR_OUT_OF_HOST_MEMORY:
		//failed, display message and return nullptr
		VKLib::exitFatalError("command pool creation failed. Out of host memory");
		break;
	case VK_ERROR_OUT_OF_DEVICE_MEMORY:
		//failed, display message and return nullptr
		VKLib::exitFatalError("command pool creation failed. Out of device memory");
		break;
	default:
		//failed, display message and return nullptr
		VKLib::exitFatalError("command pool creation failed. Unkown reason");
		break;
	}
}

void CommandBufferSetup::beginCommandBufferRecording(int commandBuffer)
{
	vkBeginCommandBuffer(this->commandBuffer[commandBuffer], &getBasicCommandBufferBeginInfo());
}

void CommandBufferSetup::endCommandBufferRecording(int commandBuffer)
{
	//end the commandBuffer
	VkResult result = vkEndCommandBuffer(this->commandBuffer[commandBuffer]);

	switch (result) {
	case VK_SUCCESS:
		//buffer created do nothing
		break;
	case VK_ERROR_OUT_OF_HOST_MEMORY:
		VKLib::exitFatalError("could not end command Buffer. Out of Host Memory");
		break;
	case VK_ERROR_OUT_OF_DEVICE_MEMORY:
		VKLib::exitFatalError("could not end command Buffer. Out of device memory");
		break;
	default:
		//failed, display message and return nullptr
		VKLib::exitFatalError("could not end command Buffer.Unkown reason");
		break;
	}
}

//this function creates a basic command buffer begin info 
VkCommandBufferBeginInfo CommandBufferSetup::getBasicCommandBufferBeginInfo()
{
	//create a handle
	VkCommandBufferBeginInfo beginInfo;

	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO; //struct type
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT; // info flag
	beginInfo.pInheritanceInfo = nullptr; // Optional
	beginInfo.pNext = VK_NULL_HANDLE;
	return beginInfo;
}