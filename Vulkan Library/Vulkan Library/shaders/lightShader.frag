#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 1) uniform sampler2D texSampler;

layout(binding = 0) uniform viewStruct {
    mat4 modelview;
    mat4 projection;
	mat4 cameraView;
	vec3 cameraPos;
} view;

layout(binding = 2) uniform lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
} light;

layout(binding = 3) uniform materialStruct {
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} material;

layout(location = 0) in vec4 fragColor;
layout(location = 1) in vec2 fragTexCoord;
layout(location = 2) in vec3 ex_V;
layout(location = 3) in vec3 ex_N;
layout(location = 4) in vec3 ex_L;


layout(location = 0) out vec4 outColor;

void main() {
    outColor = texture(texSampler, fragTexCoord);

	//ambiant 
	vec4 ambient = light.ambient * material.ambient;

	//diffuse
	vec4 diffuse = light.diffuse * material.diffuse;
	diffuse = diffuse * max(dot(normalize(ex_N),normalize(ex_L)),0);

	// Specular intensity
	// Calculate R - reflection of light
	vec3 R = normalize(reflect(normalize(-ex_L),normalize(ex_N)));
	vec4 specular = light.specular * material.specular;
	specular = specular * pow(max(dot(R,ex_V),0), material.shininess);
	vec4 textureColor = texture(texSampler, fragTexCoord) * 2;//increase the brightness of the texture
	outColor = (ambient + diffuse + specular) * textureColor;
}