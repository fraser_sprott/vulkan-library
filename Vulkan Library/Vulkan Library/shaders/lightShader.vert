#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform viewStruct {
    mat4 modelview;
    mat4 projection;
	mat4 cameraView;
	vec3 cameraPos;
} view;

layout(binding = 2) uniform lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
} light;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec4 inColor;
layout(location = 2) in vec2 inTexCoord;
layout(location = 3) in vec3 inNormal;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec2 fragTexCoord;
layout(location = 2) out vec3 ex_V;
layout(location = 3) out vec3 ex_N;
layout(location = 4) out vec3 ex_L;
out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
	vec4 vertexPosition = view.modelview * vec4(inPosition, 1.0);
	gl_Position = view.projection * vertexPosition; 

	// Find V - in eye coordinates, eye is at (0,0,0)
	ex_V = normalize(-vertexPosition).xyz;

	// surface normal in eye coordinates
	mat3 normalmatrix = transpose(inverse(mat3(view.modelview)));
	ex_N = normalize(normalmatrix * inNormal);

	// L - to light source from vertex
	ex_L = normalize(light.position.xyz - vertexPosition.xyz);


	fragColor = inColor;
    fragTexCoord = inTexCoord;
}