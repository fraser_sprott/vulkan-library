#ifndef DEVICESETUP_H
#define DEVICESETUP_H

//windows 32 program
#if defined(_WIN32)
#define VK_USE_PLATFORM_WIN32_KHR
#endif

//sdl includes 
#include "SDL.h"
#include "SDL_syswm.h"

//VKLib includes
#include "VKLib.h"

#include <algorithm>
#include <vector>
#include <set>

//use the std namespace
using namespace std;

class DeviceSetup
{
public:
	DeviceSetup();
	DeviceSetup(int screenWidth, int screenHeight);
	~DeviceSetup();
	void getAvailableWSIExtensions();
	void setupRC();
	void initialiseVulkan();
	void createVulkanSurface();
	void createPhysicalDevice();
	bool checkDeviceSuitability(VkPhysicalDevice device);
	void createLogicalDevice();
	void createSwapChain();
	void createImageView();
	void createRenderPass();

	//getters
	SDL_Window* getWindow() { return this->window; }
	VkSurfaceKHR getSurface() { return this->surface; }
	VkPhysicalDevice getPhysicalDevice(){ return this->physicalDevice; }
	VkDevice getLogicDevice() { return this->logicDevice; }
	VkQueue getQueue() { return this->queue; }
	VkQueue getPresentQueue() { return this->presentQueue; }
	VkRenderPass getRenderPass() { return this->renderPass; }
	VkExtent2D getChainExtent() { return this->chainExtent; }
	vector<VkImageView> getImageViews() { return this->imageViews; }
	VkSwapchainKHR getSwapChain() { return swapChain; }
	int getScreenWidth() { return this->screenWidth; }
	int getScreenHeight() { return this->screenHeight; }
	
private:
	//private methods
	VKLib::SwapChainSupport querySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface);
	uint32_t swapChainImageCount(VKLib::SwapChainSupport support);
	VkExtent2D chooseSwapChainExtent(const VkSurfaceCapabilitiesKHR& capabilities, int screenWidth, int screenHeight);
	VkSurfaceFormatKHR SwapChainSurfaceFormat(std::vector<VkSurfaceFormatKHR> supportedFormats);
	VkPresentModeKHR SwapChainPresentMode(vector<VkPresentModeKHR> availablePresentModes);

	//private variables
	vector<const char*> extensions;
	vector<const char*> layers;
	SDL_Window* window; 
	VkInstance instance;
	VkSurfaceKHR surface;

	//now that the devices are set up create a swap chain 
	VkSwapchainKHR swapChain;
	vector<VkImage> chainImages;
	VkFormat imageFormat;
	VkExtent2D chainExtent;
	vector<VkImageView> imageViews;

	//handle for the physical device
	VkPhysicalDevice physicalDevice;
	//create a handle for the logic device 
	VkDevice logicDevice;
	//handle for the queue
	VkQueue queue;
	//presentation queue
	VkQueue presentQueue;
	//render pass
	VkRenderPass renderPass;
	//screen dimensions
	int screenHeight;
	int screenWidth;


	const vector<const char*> deviceExtensions = {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};

};

#endif

