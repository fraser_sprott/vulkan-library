#include "GraphicPipelineSetup.h"

GraphicPipelineSetup::GraphicPipelineSetup(DeviceSetup* device, char* vertexShader, char* fragmentShader)
{
	this->device = device;
	//vertex info
	this->vertexAttibutes = VKLib::getAttributeDescriptions();
	this->vertexBinding = VKLib::getBindingDescription();
	this->vertexInfo =  VKLib::getVertexInputInfo(this->vertexBinding, this->vertexAttibutes);
	//create the descriptor set layout
	createDescriptorSetLayout(this->device->getLogicDevice());
	//create graphics pipeline
	if (VKLib::createSimpleProgram(this->device->getLogicDevice(), vertexShader, fragmentShader, device->getRenderPass(), device->getChainExtent(), this->vertexInfo, this->setLayout[0], this->pipeline, pipelineLayout, this->vertexShaderModule, this->fragmentShaderModule ) == false)
	{
		VKLib::exitFatalError("Could not create pipeline");
	}
}

GraphicPipelineSetup::~GraphicPipelineSetup()
{
	vkDestroyShaderModule(this->device->getLogicDevice(), this->vertexShaderModule, VK_NULL_HANDLE);
	vkDestroyShaderModule(this->device->getLogicDevice(), this->fragmentShaderModule, VK_NULL_HANDLE);
	vkDestroyPipeline(this->device->getLogicDevice(), this->pipeline, VK_NULL_HANDLE);
}

void GraphicPipelineSetup::createDescriptorSetLayout(VkDevice device)
{
	//create the uniform binding descriptors
	//push the projection
	this->layoutBinding.push_back(VKLib::createDescriptorSetLayoutBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT));
	//push the texture
	this->layoutBinding.push_back(VKLib::createDescriptorSetLayoutBinding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT));
	//push the light
	this->layoutBinding.push_back(VKLib::createDescriptorSetLayoutBinding(2, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_ALL));
	//push the material
	this->layoutBinding.push_back(VKLib::createDescriptorSetLayoutBinding(3, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT));
	//create the layout
	this->setLayout.resize(1);
	if (VKLib::createDescriptorSetLayout(device, this->setLayout[0], this->layoutBinding) == false)
	{
		VKLib::exitFatalError("Could not create descriptor Layout\n");
	}
}