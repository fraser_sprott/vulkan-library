//the main entry point for the vulkan library demo program
//creates a SDL window with a vulkan context

#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /ENTRY:\"mainCRTStartup\"")
#endif

//windows 32 program
#if defined(_WIN32)
#define VK_USE_PLATFORM_WIN32_KHR
#endif

//main function included. Tell SDL not to use it's main
#define SDL_MAIN_HANDLED

//includes
#include "SDL.h"
#include "SDL_syswm.h"

#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include "gtc\matrix_transform.hpp"
#include <iostream>
#include <vector>

#include "VKLibLoadBMP.h"

#include "Camera.h"
#include "DeviceSetup.h"
#include "Box.h"
#include "GraphicPipelineSetup.h"
#include "CommandBufferSetup.h"

//define the multiplication to use for degrees to radians
#define DEG_TO_RADIANS 0.017453293

//creates the render pass
void createRenderPass(VkDevice device, VkRenderPass& renderPass, VkFormat chainImageFormat)
{
	VkAttachmentDescription colorAttch;
	colorAttch.format = chainImageFormat;//match the swapchain image
	colorAttch.samples = VK_SAMPLE_COUNT_1_BIT;//multisampling basic implementiation wont use it, so set to one sample
	colorAttch.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR; //what to do with data before and after rendering,
	colorAttch.storeOp = VK_ATTACHMENT_STORE_OP_STORE; //rendered constants will be stored in memory
	colorAttch.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE; //basic implementation wont use stencils. 
	colorAttch.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE; //VK_ATTACHMENT_STORE_OP_DONT_CARE means contents of framebuffers are undefined after rendering
	colorAttch.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED; //the initial layout of the image before the renderpass starts
	colorAttch.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR; //the layout of the image to go to after the render pass is done
	colorAttch.flags = 0;

	VkAttachmentReference colorAttchRef;
	colorAttchRef.attachment = 0; //specifies what attachment to reference in an array
	colorAttchRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL; //the layout to attach to the subpass 

	VkSubpassDescription subPass = {};
	subPass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS; //specify that this is for graphics use
	subPass.colorAttachmentCount = 1; //use only one color atachment 
	subPass.pColorAttachments = &colorAttchRef; //set colorAttchRef to color attachment

	//set up the subpass dependency
	VkSubpassDependency dependency;
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.srcAccessMask = 0;
	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependency.dependencyFlags = 0;

	//create render pass 
	VkRenderPassCreateInfo passInfo;
	passInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO; //structure type 
	passInfo.attachmentCount = 1; //set 1 attachment
	passInfo.pAttachments = &colorAttch; //attach color attach
	passInfo.subpassCount = 1; //set 1 subpass
	passInfo.pSubpasses = &subPass; //pass sub pass in
	passInfo.dependencyCount = 1;
	passInfo.pDependencies = &dependency;
	passInfo.flags = 0;
	passInfo.pNext = VK_NULL_HANDLE;

	//create the render pass
	VkResult result = vkCreateRenderPass(device, &passInfo, nullptr, &renderPass);

	switch (result) {
	case VK_SUCCESS:
		cout << "Render Pass created successfully" << endl;
		break;
	case VK_ERROR_OUT_OF_HOST_MEMORY:
		cout << "Render Pass failed to create. Out of host memory" << endl;
		break;
	case VK_ERROR_OUT_OF_DEVICE_MEMORY:
		cout << "Render Pass failed to create. Out of device memory" << endl;
		break;
	default:
		cout << "Render Pass failed to create. Unkown reason" << endl;
		break;
	}
}

//creates the frame buffer
//device - logical device that the frame buffer will be created on
//renderPass - a created render pass
//chain extent - a valid chain extent
//imageView - the VkImageView vector that will be used for the buffer
//returns nullptr if failed or a valid frameBuffer vector
vector<VkFramebuffer> createFrameBuffer(VkDevice device, VkRenderPass renderPass, VkExtent2D chainExtent, vector<VkImageView> imageView)
{
	//set the size to the match the number of imageviews
	//this means there is a frame buffer for every image view
	vector<VkFramebuffer> frameBuffer;
	frameBuffer.resize(imageView.size());

	for (int i = 0; i < imageView.size(); i++) {
		//store the buffer create info
		VkFramebufferCreateInfo bufferInfo;
		bufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO; //structure type
		bufferInfo.renderPass = renderPass; //render pass
		bufferInfo.attachmentCount = 1; //number of image view adding
		bufferInfo.pAttachments = &imageView[i];
		bufferInfo.width = chainExtent.width; //buffer width - match the screen width
		bufferInfo.height = chainExtent.height; // buffer height - match the screen height
		bufferInfo.layers = 1; //number of layers
		bufferInfo.flags = 0; //set flag to 0
		bufferInfo.pNext = VK_NULL_HANDLE;
		//create the frameBuffer
		VkResult result = vkCreateFramebuffer(device, &bufferInfo, nullptr, &frameBuffer[i]);

		//check result
		switch (result) {
		case VK_SUCCESS:
			//buffer created do nothing
			break;
		case VK_ERROR_OUT_OF_HOST_MEMORY:
			//failed, display message and return nullptr
			cout << "buffer creation failed. Out of host memory" << endl;
			break;
		case VK_ERROR_OUT_OF_DEVICE_MEMORY:
			//failed, display message and return nullptr
			cout << "buffer creation failed. Out of device memory" << endl;
			break;
		default:
			//failed, display message and return nullptr
			cout << "buffer creation failed. Unkown reason" << endl;
			break;
		}
	}
	return frameBuffer;
}

//this function creates a basic command buffer begin info 
VkCommandBufferBeginInfo getBasicCommandBufferBeginInfo()
{
	//create a handle
	VkCommandBufferBeginInfo beginInfo;

	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO; //struct type
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT; // info flag
	beginInfo.pInheritanceInfo = nullptr; // Optional
	beginInfo.pNext = VK_NULL_HANDLE;
	return beginInfo;
}

//this function creates a basic renderPassBeginInfo
VkRenderPassBeginInfo getBasicRenderPassBeginInfo(VkRenderPass renderPass, vector<VkFramebuffer> frameBuffer, VkExtent2D chainExtent, int index, VkClearValue clearColor)
{
	//create a handle
	VkRenderPassBeginInfo beginInfo;

	beginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO; //struct type
	beginInfo.renderPass = renderPass; //render pass
	beginInfo.framebuffer = frameBuffer[index]; //specific frameBuffer
	beginInfo.renderArea.offset = { 0, 0 }; // offset of the data 
	beginInfo.renderArea.extent = chainExtent; // swapChain extent i.e window size
	beginInfo.clearValueCount = 1; //number of clear colors
	beginInfo.pClearValues = &clearColor; //clear color
	beginInfo.pNext = VK_NULL_HANDLE;

	//return the data
	return beginInfo;
}

bool createSemaphores(VkDevice device, VkSemaphore* imageAvailSem, VkSemaphore* renderFinishedSem)
{
	VkSemaphoreCreateInfo semaphoreCreateInfo = {};
	//at the moment api only includes sType
	semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO; //structure type
	semaphoreCreateInfo.flags = 0;
	semaphoreCreateInfo.pNext = VK_NULL_HANDLE;
	//create the semaphore
	VkResult result = vkCreateSemaphore(device, &semaphoreCreateInfo, nullptr, imageAvailSem);

	//check the result
	switch (result) {
	case VK_SUCCESS:
		//buffer created do nothing
		break;
	case VK_ERROR_OUT_OF_HOST_MEMORY:
		//failed, display message and return nullptr
		cout << "semaphore creation failed. Out of host memory" << endl;
		return false;
		break;
	case VK_ERROR_OUT_OF_DEVICE_MEMORY:
		//failed, display message and return nullptr
		cout << "semaphore creation failed. Out of device memory" << endl;
		return false;
		break;
	default:
		//failed, display message and return nullptr
		cout << "semaphore creation failed. Unkown reason" << endl;
		return false;
		break;
	}

	result = vkCreateSemaphore(device, &semaphoreCreateInfo, nullptr, renderFinishedSem);

	//check the result
	switch (result) {
	case VK_SUCCESS:
		//buffer created do nothing
		break;
	case VK_ERROR_OUT_OF_HOST_MEMORY:
		//failed, display message and return nullptr
		cout << "semaphore creation failed. Out of host memory" << endl;
		return false;
		break;
	case VK_ERROR_OUT_OF_DEVICE_MEMORY:
		//failed, display message and return nullptr
		cout << "semaphore creation failed. Out of device memory" << endl;
		return false;
		break;
	default:
		//failed, display message and return nullptr
		cout << "semaphore creation failed. Unkown reason" << endl;
		return false;
		break;
	}

	//if got here return true
	return true;
}

VKLib::lightStruct createLight()
{
	//set the light
	VKLib::lightStruct light = {
		{ (1.0f, 1.0f, 1.0f, 1.0f) }, //ambient
		{ (0.5f, 0.5f, 0.5f, 1.0f) }, //diffuse
		{ (0.5f, 0.5f ,0.5f, 1.0f) }, //specular
		{ (1.0f, 1.0f, 1.0f, 1.0f) }  //positiom
	};

	//return the light
	return light;
}

VKLib::materialStruct createMaterial()
{
	VKLib::materialStruct material = {
	{ 0.24725f,	0.2245f, 0.0645f, 1.0f }, //ambient
	{ 0.34615f, 0.3143f, 0.0903f, 1.0f }, //diffuse
	{ 0.797357f, 0.723991f, 0.208006f, 1.0f }, //specular
	  83.2f	//shininess
	};

	return material;
}

//render the frame
//indexImage is the image being returned
void renderFrame(VkDevice device, VkSwapchainKHR swapChain, VkSemaphore imageAvailable, VkSemaphore renderFinished, vector<VkCommandBuffer> buffers, VkQueue queue)
{
	uint32_t imageIndex; //the image index to be drawn
	uint64_t timeout = numeric_limits<uint64_t>::max();//setting the timeout parameter to the largest value disables it  

	//acquire the next image
	vkAcquireNextImageKHR(device, swapChain, timeout, imageAvailable, VK_NULL_HANDLE, &imageIndex);

	//create the submit info 
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

	VkSemaphore waitSems[] = { imageAvailable }; //need to store the image available in array structure
	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT };//wait till the image available
	submitInfo.waitSemaphoreCount = 1; //how many semaphores used
	submitInfo.pWaitSemaphores = waitSems;
	submitInfo.pWaitDstStageMask = waitStages;
	submitInfo.commandBufferCount = 1; //the number of command buffers being passed in
	submitInfo.pCommandBuffers = &buffers[imageIndex];//pass the commandBuffer Index into it
	VkSemaphore signalSemaphores[] = { renderFinished }; //specifies what semaphores to signal once finished executing
	submitInfo.signalSemaphoreCount = 1; //the number of semaphores
	submitInfo.pSignalSemaphores = signalSemaphores; //signal
	submitInfo.pNext = VK_NULL_HANDLE;

	//submit the command buffer to the queue
	VkResult result = vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);

	if (result != VK_SUCCESS)
	{
		VKLib::exitFatalError("Could not get submit info");
	}

	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = signalSemaphores;

	VkSwapchainKHR swapChains[] = { swapChain };
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapChains;
	presentInfo.pImageIndices = &imageIndex;
	presentInfo.pResults = nullptr; // Optional
	presentInfo.pNext = VK_NULL_HANDLE;

	vkQueuePresentKHR(queue, &presentInfo);
}

void updateView(VkDevice device, VkDeviceMemory &bufferMemory, VKLib::viewStruct &views)
{
	//rotate the model
	views.modelView = glm::mat4(1.0f);
	views.modelView = glm::translate(views.modelView, glm::vec3(0.0f, 0.0f, 0.0f));
	views.modelView = glm::scale(views.modelView, glm::vec3(0.5f, 0.5f, 0.5f));

	//update the buffer
	VKLib::updateViewUniformBuffer(device, bufferMemory, views);
}

void handleEvents(Camera &camera) {
	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	if (keys[SDL_SCANCODE_W]) camera.moveForward(-0.1f);
	if (keys[SDL_SCANCODE_S]) camera.moveForward(0.1f);
	if (keys[SDL_SCANCODE_A]) camera.moveRight(-0.1f);
	if (keys[SDL_SCANCODE_D]) camera.moveRight(0.1f);
	if (keys[SDL_SCANCODE_R]) camera.moveUp(-0.1);
	if (keys[SDL_SCANCODE_F]) camera.moveUp(0.1);
	if (keys[SDL_SCANCODE_COMMA])  camera.rotate(1.0f);
	if (keys[SDL_SCANCODE_PERIOD])  camera.rotate(-1.0f);
}

int main(int argv[], char* args)
{
	bool running = true;
	
	//screen width, screen height
	const int screenWidth = 1024;
	const int screenHeight = 768;

	//set the camera
	Camera camera = Camera(glm::vec3(-2.0, 2, -4), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f), 0.0f);
	//create the device, window and surface
	DeviceSetup deviceSetup = DeviceSetup(screenWidth, screenHeight);
	//create the graphics pipeline
	GraphicPipelineSetup graphicsPipeline = GraphicPipelineSetup(&deviceSetup, "shaders/lightShader.vert", "shaders/lightShader.frag");
	//create the command buffer
	CommandBufferSetup commandBuffer = CommandBufferSetup(&deviceSetup);
	//box 
	Box box = Box(&deviceSetup);

	//uniform creation	
	VKLib::viewStruct views;
	VkBuffer viewBuffer;
	VkDeviceMemory viewBufferMemory;
	VKLib::createUniformViewBuffer(deviceSetup.getLogicDevice(), deviceSetup.getPhysicalDevice(), viewBuffer, viewBufferMemory);

	//create the light uniform
	VKLib::lightStruct light = createLight();
	VkBuffer lightBuffer;
	VkDeviceMemory lightBufferMemory;
	VKLib::createUniformLightBuffer(deviceSetup.getLogicDevice(), deviceSetup.getPhysicalDevice(), lightBuffer, lightBufferMemory);

	//create the material uniform
	VKLib::materialStruct material = createMaterial();
	VkBuffer materialBuffer;
	VkDeviceMemory materialBufferMemory;
	VKLib::createUniformMaterialBuffer(deviceSetup.getLogicDevice(), deviceSetup.getPhysicalDevice(), materialBuffer, materialBufferMemory);

	//pool size information
	vector<VkDescriptorPoolSize> poolSize;
	poolSize.resize(4);
	poolSize[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSize[0].descriptorCount = 1;
	poolSize[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	poolSize[1].descriptorCount = 1;
	poolSize[2].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSize[2].descriptorCount = 1;
	poolSize[3].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSize[3].descriptorCount = 1;

	//handles for the descriptor pool and set
	VkDescriptorPool descriptorPool;
	vector<VkDescriptorSet> descriptorSet;
	descriptorSet.resize(1);
	
	//create the descripor pool and set
	VKLib::createDescriptorPool(deviceSetup.getLogicDevice(), poolSize, 1, descriptorPool);
	VKLib::createDescriptorSet(deviceSetup.getLogicDevice(), descriptorPool, graphicsPipeline.getSetLayout(), 1, descriptorSet[0]);

	//load the texture
	VKLib::imageStages image;
	if (VKLib::loadTexture(deviceSetup.getPhysicalDevice(), deviceSetup.getLogicDevice(), "textures/tile_1.bmp", image, VK_FORMAT_R8G8B8A8_UNORM) == false)
	{
		VKLib::exitFatalError("Could not load image");
	}

	//create the imageView 
	VkImageView textureImageView;
	if (VKLib::createImageView(deviceSetup.getLogicDevice(), textureImageView, image.texture, VK_FORMAT_R8G8B8A8_UNORM) == false)
	{
		VKLib::exitFatalError("Could not create textured image view\n");
	}

	//create the texture sampler
	VkSampler textureSampler;
	if (VKLib::createTextureSampler(deviceSetup.getLogicDevice(), textureSampler) == false)
	{
		VKLib::exitFatalError("could not create texture sampler\n");
	}

	//descriptor buffer info
	VkDescriptorBufferInfo viewBufferInfo;
	viewBufferInfo.buffer = viewBuffer;
	viewBufferInfo.offset = 0;
	viewBufferInfo.range = sizeof(VKLib::viewStruct);

	//descriptor buffer info
	VkDescriptorBufferInfo lightBufferInfo;
	lightBufferInfo.buffer = lightBuffer;
	lightBufferInfo.offset = 0;
	lightBufferInfo.range = sizeof(VKLib::lightStruct);

	//descriptor buffer info
	VkDescriptorBufferInfo materialBufferInfo;
	materialBufferInfo.buffer = materialBuffer;
	materialBufferInfo.offset = 0;
	materialBufferInfo.range = sizeof(VKLib::materialStruct);

	//update the uniforms 
	VKLib::updateUniformDescriptorSets(deviceSetup.getLogicDevice(), viewBufferInfo, descriptorSet[0], 0, 0, 1);//views
	VKLib::updateUniformDescriptorSets(deviceSetup.getLogicDevice(), lightBufferInfo, descriptorSet[0], 2, 0, 1);//light
	VKLib::updateUniformDescriptorSets(deviceSetup.getLogicDevice(), materialBufferInfo, descriptorSet[0], 3, 0, 1);//material
	VKLib::updateImageDescriptorSets(deviceSetup.getLogicDevice(), textureImageView, textureSampler, descriptorSet[0], 1, 0, 1);

	//prepare the texture
	VKLib::prepareTextureImage(deviceSetup.getLogicDevice(), commandBuffer.getCommandPool(), deviceSetup.getQueue(), image);

	//setup the command buffer recording and start the render pass//loop for each command buffer
	for (int i = 0; i < commandBuffer.getCommandBuffers().size(); i++)
	{
		//begin the command buffer
		vkBeginCommandBuffer(commandBuffer.getCommandBuffer(i), &getBasicCommandBufferBeginInfo());

		//get the begin info takes the renderpass, frameBuffer, chainExtent, index of the frameBuffer, and the clear color  
		VkRenderPassBeginInfo info = getBasicRenderPassBeginInfo(deviceSetup.getRenderPass(), commandBuffer.getFrameBuffers(), deviceSetup.getChainExtent(), i, VkClearValue{ 0.5f, 0.5f, 0.5f, 1.0f });
		//begin the render pass. VK_SUBPASS_CONTENTS_INLINE specifies that it will be executed from the primary buffers
		vkCmdBeginRenderPass(commandBuffer.getCommandBuffer(i), &info, VK_SUBPASS_CONTENTS_INLINE);

		//bind it to the pipeline //VK_PIPELINE_BIND_POINT_GRAPHICS specifies that it is a graphics pipeline
		vkCmdBindPipeline(commandBuffer.getCommandBuffer(i), VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline.getPipeline());
		//draw the mesh buffer contents
		box.drawMesh(commandBuffer.getCommandBuffer(i), graphicsPipeline.getPipelineLayout(), descriptorSet);
		//end the render Pass
		vkCmdEndRenderPass(commandBuffer.getCommandBuffer(i));

		//end the commandBuffer
		VkResult result = vkEndCommandBuffer(commandBuffer.getCommandBuffer(i));

		switch (result) {
		case VK_SUCCESS:
			//buffer created do nothing
			break;
		case VK_ERROR_OUT_OF_HOST_MEMORY:
			VKLib::exitFatalError("could not end command Buffer. Out of Host Memory");
			break;
		case VK_ERROR_OUT_OF_DEVICE_MEMORY:
			VKLib::exitFatalError("could not end command Buffer. Out of device memory");
			break;
		default:
			//failed, display message and return nullptr
			VKLib::exitFatalError("could not end command Buffer.Unkown reason");
			break;
		}
	}

	//create sephmores to ensure syncronization
	VkSemaphore imageAvailableSem, renderFinishedSem; //create the holders
	if (createSemaphores(deviceSetup.getLogicDevice(), &imageAvailableSem, &renderFinishedSem) == false)
	{
		VKLib::exitFatalError("Could not create Semaphores");
	}

	float fov = 45.0f*DEG_TO_RADIANS;
	float aspect = (screenWidth / screenHeight);
	float znear = 0.0f;
	float zfar = 100.0f;

	//start the render pass
	while (running)
	{
		SDL_Event event;

		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				running = false;

			handleEvents(camera);
			views.cameraView = camera.getCameraMatrix();
			//not ideal but only way box displays //this needs to be looked at 
			views.projection = glm::perspective(fov, aspect, znear, zfar) * views.cameraView;
			views.cameraPos = camera.getPos();
			//update
			//updateView(deviceSetup.getLogicDevice(), viewBufferMemory);
			box.updateModelView(viewBufferMemory, views);

			//update the light
			VKLib::updateLightUniformBuffer(deviceSetup.getLogicDevice(), lightBufferMemory, light);
			//update the material
			VKLib::updateMaterialUniformBuffer(deviceSetup.getLogicDevice(), materialBufferMemory, material);
			
			//render
			renderFrame(deviceSetup.getLogicDevice(), deviceSetup.getSwapChain(), imageAvailableSem, renderFinishedSem, commandBuffer.getCommandBuffers(), deviceSetup.getQueue());
		}
		SDL_Delay(1);
	} 

	//wait for the device to finish
	vkDeviceWaitIdle(deviceSetup.getLogicDevice());
	//Clean up
	SDL_Quit();
	return 0;
}