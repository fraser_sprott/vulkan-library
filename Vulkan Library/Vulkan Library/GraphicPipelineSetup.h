#ifndef GRAPHICPIPELINESETUP_H
#define GRAPHICPIPELINESETUP_H

#include "VKLib.h"
#include "VKLibShader.h"
#include "VKLibVertex.h"
#include "DeviceSetup.h"

class GraphicPipelineSetup
{
public:
	GraphicPipelineSetup() {}
	GraphicPipelineSetup(DeviceSetup* device, char* vertexShader, char* fragmentShader);
	~GraphicPipelineSetup();
	void createDescriptorSetLayout(VkDevice device);

	//getter
	vector<VkDescriptorSetLayout> getSetLayout() { return setLayout; }
	VkPipeline getPipeline() { return pipeline; }
	VkPipelineLayout getPipelineLayout() { return pipelineLayout; }
private:
	//handle to the deviceSetup
	DeviceSetup* device;
	//vertex binding info
	vector<VkVertexInputAttributeDescription>  vertexAttibutes;
	vector<VkVertexInputBindingDescription> vertexBinding; 
	VkPipelineVertexInputStateCreateInfo vertexInfo;
	//create the uniform binding descriptors
	vector<VkDescriptorSetLayoutBinding> layoutBinding;
	//create the layout
	vector<VkDescriptorSetLayout> setLayout;
	//pipeline handle
	VkPipeline pipeline;
	VkPipelineLayout pipelineLayout;
	VkShaderModule vertexShaderModule;
	VkShaderModule fragmentShaderModule;
};

#endif