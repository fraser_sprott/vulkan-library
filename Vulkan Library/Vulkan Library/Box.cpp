#include "Box.h"



Box::Box(DeviceSetup* device)
{
	this->device = device;
	this->mesh = VKLib::createMesh(this->device->getLogicDevice(), this->device->getPhysicalDevice(), vertices, indices);

}


Box::~Box()
{
}

void Box::drawMesh(VkCommandBuffer commandBuffer, VkPipelineLayout pipelineLayout, std::vector<VkDescriptorSet> descriptorSet)
{
	VKLib::drawMesh(commandBuffer, 0, pipelineLayout, descriptorSet);
}

void Box::updateModelView(VkDeviceMemory viewBufferMemory, VKLib::viewStruct& views)
{
	//translate the model
	views.modelView = glm::mat4(1.0f);
	views.modelView = glm::translate(views.modelView, glm::vec3(0.0f, 0.0f, 0.0f));
	views.modelView = glm::scale(views.modelView, glm::vec3(0.5f, 0.5f, 0.5f));

	//update the buffer
	VKLib::updateViewUniformBuffer(this->device->getLogicDevice(), viewBufferMemory, views);

}