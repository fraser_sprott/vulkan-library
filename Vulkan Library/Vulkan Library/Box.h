#ifndef BOX_H
#define BOX_H

#include "glm.hpp"
#include "gtc\matrix_transform.hpp"
#include "VKLib.h"
#include "VKLibVertex.h"
#include "DeviceSetup.h"

#include <vector>

class Box
{
public:
	Box(){}
	Box(DeviceSetup* device);
	~Box();
	void updateModelView(VkDeviceMemory viewBufferMemory, VKLib::viewStruct &views);
	void Box::drawMesh(VkCommandBuffer commandBuffer, VkPipelineLayout pipelineLayout, std::vector<VkDescriptorSet> descriptorSet);
private:
	std::vector<VKLib::vertex> vertices = {
		//position							//colour						//texture coordinates	 //normals
		{ glm::vec3(-1.0f, -1.0f, -1.0f), glm::vec4(1.0f, 0.0f, 0.0f, 1.0f), glm::vec2(0.0f, 0.0f), glm::vec3(-1.0f, -1.0f, -1.0f) },
		{ glm::vec3(-1.0, 1.0f, -1.0f), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f), glm::vec2(0.0f, 1.0f), glm::vec3(-1.0, 1.0f, -1.0f) },
		{ glm::vec3(1.0, 1.0f, -1.0f), glm::vec4(0.0f, 0.0f, 1.0f, 1.0f), glm::vec2(1.0f, 1.0f), glm::vec3(1.0, 1.0f, -1.0f) },
		{ glm::vec3(1.0, -1.0f, -1.0f), glm::vec4(1.0f, 0.0f, 1.0f, 1.0f), glm::vec2(1.0f, 0.0f), glm::vec3(1.0, -1.0f, -1.0f) },
		{ glm::vec3(-1.0, -1.0f, 1.0f), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f), glm::vec2(1.0f, 0.0f), glm::vec3(-1.0, -1.0f, 1.0f) },
		{ glm::vec3(-1.0, 1.0f, 1.0f), glm::vec4(1.0f, 0.0f, 0.0f, 1.0f), glm::vec2(1.0f, 1.0f), glm::vec3(-1.0, 1.0f, 1.0f) },
		{ glm::vec3(1.0, 1.0f, 1.0f), glm::vec4(0.0f, 0.0f, 1.0f, 1.0f), glm::vec2(0.0f, 1.0f), glm::vec3(1.0, 1.0f, 1.0f) },
		{ glm::vec3(1.0, -1.0f, 1.0f), glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), glm::vec2(0.0f, 0.0f), glm::vec3(1.0, -1.0f, 1.0f) }
	};

	const std::vector<uint16_t> indices = {
		0,1,2, 0,2,3, // back  
		1,0,5, 0,4,5, // left					
		6,3,2, 3,6,7, // right
		1,5,6, 1,6,2, // top
		0,3,4, 3,7,4, // bottom
		6,5,4, 7,6,4 // front
	};

	//stores the device
	DeviceSetup* device;
	//stores the mesh data
	int mesh;
};

#endif
