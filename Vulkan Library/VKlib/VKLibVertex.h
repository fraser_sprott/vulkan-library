#ifndef VKLIBVERTEX_H
#define VKLIBVERTEX_H

#include "VKLib.h"
#include "glm.hpp"
#include <map>
using namespace std;

namespace VKLib {
	//helpful structure that stores vertex information
	struct vertex{
		glm::vec3 position;//3D object
		glm::vec4 color; //rgba
		glm::vec2 texCoords; //texture coordinates
		glm::vec3 normals; //normals
	};		
	
	//return a basic vertex attribute description// this is needed for vertex in data
	vector<VkVertexInputAttributeDescription> getAttributeDescriptions();

	//returns the vertexInputBindingDescription with the size set to the vertex in the library
	vector<VkVertexInputBindingDescription> getBindingDescription();

	//returns the vertexInputStrateCreateInfo
	VkPipelineVertexInputStateCreateInfo  getVertexInputInfo(vector<VkVertexInputBindingDescription> &vertexBinding, vector<VkVertexInputAttributeDescription> &attributes);

	/*finds a suitable memory type
	filter - memory requirement flag bits
	propertyFlags - property flags
	device - Physical device
	returns the number of suitable memory, returns -1 on failure
	*/
	int findSuitableMemoryType(int filter, VkMemoryPropertyFlags propertyFlags, VkPhysicalDevice device);

	/*creates a mesh using passed in vertices and indices
	device - valid logic device
	physicalDevice - valid physical device
	vertices - the vertices of the object being created
	inidices - the indices of the object
	returns the mesh id to be used with the draw mesh function
	*/
	int createMesh(VkDevice device, VkPhysicalDevice physicalDevice, vector<vertex> &vertices, vector<uint16_t> indices);
	
	/*draws the mesh with no uniform data
	commandBuffer - command buffer to draw with
	meshID - the id of the mesh to be drawn
	*/
	void drawMesh(VkCommandBuffer &commandBuffer, const int meshID);
	
	/*draws the mesh with uniform data
	commandBuffer - command buffer to draw with
	meshID - the id of the mesh to be drawn
	pipelineLayout - valid pipeline layout 
	descriptorSet - dvector od descriptor sets for the uniform
	*/
	void drawMesh(VkCommandBuffer &commandBuffer, const int meshID, VkPipelineLayout pipelineLayout, vector<VkDescriptorSet> descriptorSet);
}


#endif

