#include "VKLibDefaults.h"

namespace VKLibDefaults 
{
	VkPipelineVertexInputStateCreateInfo getDefaultVertexInputInfo()
	{
		VkPipelineVertexInputStateCreateInfo vertexInput;
		vertexInput.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO; //type info
		vertexInput.vertexBindingDescriptionCount = 0; //no vertex data to load
		vertexInput.pVertexBindingDescriptions = nullptr; // Optional
		vertexInput.vertexAttributeDescriptionCount = 0; //no vertex data to load
		vertexInput.pVertexAttributeDescriptions = nullptr; // Optional
		vertexInput.flags = 0;
		vertexInput.pNext = VK_NULL_HANDLE;
		return vertexInput;
	}

	VkPipelineInputAssemblyStateCreateInfo getDefaultInputAssemblyInfo()
	{
		VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo;
		inputAssemblyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO; //type identifier
		inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST; //specifies to use 3 vertices for a triangle without reuse
		inputAssemblyInfo.primitiveRestartEnable = VK_FALSE;
		inputAssemblyInfo.flags = 0;
		inputAssemblyInfo.pNext = VK_NULL_HANDLE;
		return inputAssemblyInfo;
	}

	VkViewport getDefaultViewport(VkExtent2D chainExtent)
	{
		VkViewport viewport;
		//this sets the top left of the screen to coord (0,0)
		viewport.x = 0.0f;//left coordinate
		viewport.y = 0.0f;//top coordinate

		 //since the chain extent holds the actual image size set the viewport size to match
		viewport.width = (float)chainExtent.width; //screen width 
		viewport.height = (float)chainExtent.height; //screen height

		//depth of theviewport must be between 0 and 1
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;

		//return viewport
		return viewport;
	}

	VkRect2D getDefaultScissor(VkExtent2D chainExtent) {
		VkRect2D scissor;

		scissor.offset = { 0, 0 };
		scissor.extent = chainExtent;

		return scissor;
	}

	VkPipelineViewportStateCreateInfo getDefaultViewportStateInfo(VkViewport viewport,VkRect2D scissor) {
		VkPipelineViewportStateCreateInfo viewportStateInfo;
		//creates the viewportStateInfo using the passed in viewport and scissor
		viewportStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO; // structure type
		viewportStateInfo.viewportCount = 1; //the number of viewports //this function only allows one
		viewportStateInfo.pViewports = &viewport; //pass the viewport to it
		viewportStateInfo.scissorCount = 1; //the number of viewports //this function only allows one
		viewportStateInfo.pScissors = &scissor;//pass the scissor to it
		viewportStateInfo.flags = 0; //optional
		viewportStateInfo.pNext = VK_NULL_HANDLE; 
		return viewportStateInfo;
	}

	VkPipelineRasterizationStateCreateInfo getDefaultRasterizationStateInfo()
	{
		 VkPipelineRasterizationStateCreateInfo rasterizationInfo;
		//the rasterization stage specifies how to clip and render geomertry
		rasterizationInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		rasterizationInfo.depthClampEnable = VK_FALSE;// true means any fragment outside clipping plane is clamped instead of removed
		rasterizationInfo.rasterizerDiscardEnable = VK_FALSE; //need to be false to allow output to the framebuffers
		rasterizationInfo.polygonMode = VK_POLYGON_MODE_FILL; //fill in the polygons
		rasterizationInfo.lineWidth = 1.0f; //the widht of the lines highest is 1.0f unless a gpu feature is enabled
		rasterizationInfo.cullMode = VK_CULL_MODE_BACK_BIT;	//cull the back of objects
		rasterizationInfo.frontFace = VK_FRONT_FACE_CLOCKWISE;	//specifies the order of which culling is done clockwise or anticlockwise
		rasterizationInfo.depthBiasEnable = VK_FALSE;	//turn depth bias off. There are a few more parts but since this is false they are not needed
		
		//the rest is optional
		rasterizationInfo.depthBiasConstantFactor = 0.0f;
		rasterizationInfo.depthBiasClamp = 0.0f;
		rasterizationInfo.depthBiasSlopeFactor = 0.0f;
		rasterizationInfo.flags = 0;
		rasterizationInfo.pNext = VK_NULL_HANDLE;

		return rasterizationInfo;
	}

	VkPipelineMultisampleStateCreateInfo getDefaultMultisampleStageInfo()
	{
			VkPipelineMultisampleStateCreateInfo multisamplingInfo;
			//due to the need of extensions being enabled for multisampling turn set multi sampling to off as there is no guarantee that the application will have it setup
			multisamplingInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;//
			multisamplingInfo.sampleShadingEnable = VK_FALSE;
			multisamplingInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
			multisamplingInfo.pSampleMask = VK_NULL_HANDLE;
			multisamplingInfo.flags = 0;
			multisamplingInfo.pNext = VK_NULL_HANDLE;
			return multisamplingInfo;
	}

	VkPipelineColorBlendAttachmentState getDefaultColorBlendAttachment()
	{
		VkPipelineColorBlendAttachmentState colorBlendAtch;
		//use alpha blending as the default. use the alpha minus one version
		colorBlendAtch.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		colorBlendAtch.blendEnable = VK_TRUE;
		colorBlendAtch.colorBlendOp = VK_BLEND_OP_ADD; // Optional
		colorBlendAtch.alphaBlendOp = VK_BLEND_OP_ADD; // Optional
		colorBlendAtch.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_COLOR; // Optional
		colorBlendAtch.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
		colorBlendAtch.srcAlphaBlendFactor = VK_BLEND_FACTOR_SRC_COLOR; // Optional
		colorBlendAtch.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
		return colorBlendAtch;
	}

	//takes a color blend attachment
	VkPipelineColorBlendStateCreateInfo getDefaultColorBlendInfo(VkPipelineColorBlendAttachmentState colorBlendAtch)
	{
		VkPipelineColorBlendStateCreateInfo colorBlendInfo;
		colorBlendInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		colorBlendInfo.logicOpEnable = VK_FALSE;
		colorBlendInfo.logicOp = VK_LOGIC_OP_COPY; // Optional
		colorBlendInfo.attachmentCount = 1; 
		colorBlendInfo.pAttachments = &colorBlendAtch;
		colorBlendInfo.blendConstants[0] = 0.0f; // Optional
		colorBlendInfo.blendConstants[1] = 0.0f; // Optional
		colorBlendInfo.blendConstants[2] = 0.0f; // Optional
		colorBlendInfo.blendConstants[3] = 0.0f; // Optional
		colorBlendInfo.flags = 0;
		colorBlendInfo.pNext = VK_NULL_HANDLE;

		return colorBlendInfo;
	}

	VkPipelineDynamicStateCreateInfo getDefaultDynamicStateInfo()
	{
		VkDynamicState dynamicStates[] = {VK_DYNAMIC_STATE_LINE_WIDTH};

		VkPipelineDynamicStateCreateInfo dynamicState;
		//as they have not dont use any 
		dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO; //type of info
		dynamicState.dynamicStateCount = 0; 
		dynamicState.pDynamicStates = nullptr;
		dynamicState.flags = 0;
		dynamicState.pNext = VK_NULL_HANDLE;

		return dynamicState;
	}

	VkPipelineLayoutCreateInfo getDefaultLayoutInfo()
	{
		VkPipelineLayoutCreateInfo layoutInfo;
		layoutInfo.flags = 0; //set to 0
		layoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		layoutInfo.setLayoutCount = 0; //number of descriptor sets included
		layoutInfo.pSetLayouts = 0; //descriptor sets
		layoutInfo.pushConstantRangeCount = 0; //number of push constant ranges included 
		layoutInfo.pPushConstantRanges = 0; //the push constant ranges
		layoutInfo.pNext = VK_NULL_HANDLE;
		return layoutInfo;
	}
}