#include "VKLibLoadBMP.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

//the define prevents linking errors
namespace VKLib
{
	bool createImage(VkPhysicalDevice physicalDevice, VkDevice device, int width, int height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage &image, VkDeviceMemory &imageMemory)
	{
		//create the image create info
		VkImageCreateInfo imgInfo;
		imgInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO; //structure type
		imgInfo.imageType = VK_IMAGE_TYPE_2D; //type of image
		imgInfo.extent.width = width; //width of the texture
		imgInfo.extent.height = height; //height of the texture
		imgInfo.extent.depth = 1; //used for texels per dimension, needs to be 1
		imgInfo.mipLevels = 1; //no mipmaps
		imgInfo.arrayLayers = 1; //not an array
		imgInfo.format = format; //allow the user to specify
		imgInfo.tiling = tiling; //tiling depends on image user specified
		imgInfo.initialLayout = VK_IMAGE_LAYOUT_PREINITIALIZED;//used for textures
		imgInfo.usage = usage; //usage is passed in
		imgInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE; // only use with one queue family
		imgInfo.samples = VK_SAMPLE_COUNT_1_BIT; //1 sample higher for multisampling
		imgInfo.flags = 0; // Optional
		imgInfo.pNext = VK_NULL_HANDLE; //reserved for future use

										//try to create the image return false if failed
		if (vkCreateImage(device, &imgInfo, nullptr, &image) != VK_SUCCESS) {
			cout << "failed to create Image\n";
			return false;
		}

		//get the memory requirments for the image
		VkMemoryRequirements requirements;
		vkGetImageMemoryRequirements(device, image, &requirements);

		//fill the memory allocate info structure
		VkMemoryAllocateInfo allocInfo;
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = requirements.size;
		allocInfo.memoryTypeIndex = findSuitableMemoryType(requirements.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, physicalDevice);
		allocInfo.pNext = VK_NULL_HANDLE;
		//try to allocate the memory, return false if failed
		if (vkAllocateMemory(device, &allocInfo, nullptr, &imageMemory) != VK_SUCCESS) {
			cout << "failed to allocate memory\n";
			return false;
		}

		//bind the memory
		vkBindImageMemory(device, image, imageMemory, 0);

		return true;
	}

	//takes a bmp image in and loads it in from the file specified
	//calls createImage inside
	//returns a imageStages struct which contains the staging image and the texture 
	bool loadTexture(VkPhysicalDevice physicalDevice, VkDevice device, char* filename, imageStages &image, VkFormat format)
	{
		int width;//textures width
		int height;//textures height
		int channels; //texture channels

					  //loads the file //STBI_rgb_alpha forces the image to load with alpha
		stbi_uc* pixelInput = stbi_load(filename, &width, &height, &channels, STBI_rgb_alpha);

		//set the images width and height
		image.dimension.width = width;
		image.dimension.height = height;

		//4bytes per pixel so to get the size multiply width and height
		VkDeviceSize imageSize = width * height * 4;

		//if something went wrong return false
		if (!pixelInput) {
			cout << "could not load image\n";
			return false;
		}

		//create the image //return false if it didnt create image
		if (createImage(physicalDevice, device, width, height, format, VK_IMAGE_TILING_LINEAR, VK_IMAGE_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, image.stagingImage, image.stagingMemory) == false)
		{
			cout << "Could not create staging Image\n";
			return false;
		}

		//map the memory
		void* data;
		vkMapMemory(device, image.stagingMemory, 0, imageSize, 0, &data);

		//before the data gets copied to the GPU, check that it can read the data
		//as some cards might expect extra bytes
		VkImageSubresource subresource;
		subresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		subresource.mipLevel = 0; //no mipmapping
		subresource.arrayLayer = 0;//no array

								   //get the subresourceLayout for the image
		VkSubresourceLayout subresourceLayout;
		vkGetImageSubresourceLayout(device, image.stagingImage, &subresource, &subresourceLayout);

		//copy the image// the first is when no paddign bytes are needed
		if (subresourceLayout.rowPitch == width * 4) {
			memcpy(data, pixelInput, (size_t)imageSize);
		}
		else {
			//padding bytes have to be added, bit more work
			uint8_t* dataBytes = reinterpret_cast<uint8_t*>(data);
			//loop for each of the rows
			for (int y = 0; y < height; y++) {
				memcpy(&dataBytes[y * subresourceLayout.rowPitch], &pixelInput[y * width * 4], width * 4);
			}
		}

		//unmap the memory
		vkUnmapMemory(device, image.stagingMemory);

		//free the image in the library
		stbi_image_free(pixelInput);

		//create the image that will be displayed
		if (createImage(physicalDevice, device, width, height, format, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, image.texture, image.textureMemory) == false)
		{
			cout << "Could not create texture\n";
			return false;
		}

		//return true
		return true;
	}

	//used for setting up the image for a commandBuffer
	void transitionImageLayout(VkCommandBuffer &commandBuffer, VkImage &image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout)
	{
		//used for setting up the layoutTransition
		VkImageMemoryBarrier barrier;
		barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.oldLayout = oldLayout; //old layout, to be replaced
		barrier.newLayout = newLayout; //new layout to add
		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED; //not used to transfer ownership
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED; //not used to transfer ownership
		barrier.image = image; //the image that is affected
		barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		barrier.subresourceRange.baseMipLevel = 0; //no mipmaps
		barrier.subresourceRange.levelCount = 1; //so specify 1 layer
		barrier.subresourceRange.baseArrayLayer = 0;//same for the array
		barrier.subresourceRange.layerCount = 1; //number of layers
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = 0;

		//create the pipelineBarrier
		vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);
	}

	//copies the staging image to the command buffer
	void copyImage(VkCommandBuffer &commandBuffer, imageStages &image)
	{
		VkImageSubresourceLayers subresource;
		subresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		subresource.baseArrayLayer = 0; //
		subresource.mipLevel = 0; //no mipmap
		subresource.layerCount = 1; //single layer

		VkImageCopy region;
		region.srcSubresource = subresource; //source VkImageSubresourceLayers
		region.dstSubresource = subresource; //destination VkImageSubresourceLayers
		region.srcOffset = { 0, 0, 0 }; //offsets, set to none
		region.dstOffset = { 0, 0, 0 }; //offsets, set to none
		region.extent.width = image.dimension.width; //image width
		region.extent.height = image.dimension.height; //image height
		region.extent.depth = 1; //image depth

		//copy the image
		vkCmdCopyImage(commandBuffer, image.stagingImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, image.texture, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
	}

	//used by the  prepareTextureImage this function sets up a command buffer to run once as copy image shouldnt
	//be used during a renderPass
	void startCommand(VkDevice device, VkCommandBuffer &commandBuffer, VkCommandPool &commandPool) {
		VkCommandBufferAllocateInfo allocInfo;
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO; //structure type
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY; //type of command buffer
		allocInfo.commandPool = commandPool; //command pool
		allocInfo.commandBufferCount = 1; //number of command buffers
		allocInfo.pNext = VK_NULL_HANDLE;
		//allocate the command buffer
		vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);

		VkCommandBufferBeginInfo beginInfo;
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;//structure type 
		beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT; //flags. specify that this is to be used once
		beginInfo.pNext = VK_NULL_HANDLE;
		beginInfo.pInheritanceInfo = VK_NULL_HANDLE;
		//begin recording
		vkBeginCommandBuffer(commandBuffer, &beginInfo);
	}

	//used by the  prepareTextureImage this function ends a command buffer that is only runnign once as copy image shouldnt
	//be used during a renderPass
	void endCommand(VkDevice device, VkCommandBuffer &commandBuffer, VkQueue &graphicsQueue, VkCommandPool &commandPool) {
		//end the pass
		vkEndCommandBuffer(commandBuffer);

		//submmit info
		VkSubmitInfo submitInfo;
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO; //structure type
		submitInfo.commandBufferCount = 1;//number of command buffers 
		submitInfo.pCommandBuffers = &commandBuffer; //command buffer
		submitInfo.pNext = VK_NULL_HANDLE; //reserved for future use
		submitInfo.signalSemaphoreCount = 0; //no signal semaphores
		submitInfo.pSignalSemaphores = VK_NULL_HANDLE;
		submitInfo.waitSemaphoreCount = 0; //no wait semaphores
		submitInfo.pWaitSemaphores = VK_NULL_HANDLE;
		submitInfo.pWaitDstStageMask = VK_NULL_HANDLE;

		//submit the graphics queue
		vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
		vkQueueWaitIdle(graphicsQueue);

		//free the commands
		vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
	}

	//this function prepares the texture so that the command buffers can transfer the staging image to the texture
	//this function handles the 
	void prepareTextureImage(VkDevice device, VkCommandPool commandPool, VkQueue graphicsQueue, imageStages image)
	{
		//setUp a command buffer
		VkCommandBuffer commandBuffer;
		// start recording
		startCommand(device, commandBuffer, commandPool);
		//transition the images
		transitionImageLayout(commandBuffer, image.stagingImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_PREINITIALIZED, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
		transitionImageLayout(commandBuffer, image.texture, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_PREINITIALIZED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
		//copy the images
		copyImage(commandBuffer, image);
		//transition the texture again
		transitionImageLayout(commandBuffer, image.texture, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		//end recording
		endCommand(device, commandBuffer, graphicsQueue, commandPool);
	}


	//creates an image view, can be used to create the image view during the setup of vulkan
	//used to setup the image view for a texture
	//if using the imageStages struct pass the texture variable to achive this
	//returns false if failed to create
	bool createImageView(VkDevice device, VkImageView &imageView, VkImage &image, VkFormat format)
	{
		VkImageViewCreateInfo viewInfo = {};
		viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO; //sturcture type
		viewInfo.image = image;//image
		viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;//view type
		viewInfo.format = format; //format
		viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT; //aspect mask
		viewInfo.subresourceRange.baseMipLevel = 0;//no mipmap
		viewInfo.subresourceRange.levelCount = 1;//single layer
		viewInfo.subresourceRange.baseArrayLayer = 0; //no array
		viewInfo.subresourceRange.layerCount = 1; //singler layer

												  //create the image view
		if (vkCreateImageView(device, &viewInfo, nullptr, &imageView) != VK_SUCCESS) {
			cout << "could not create the image view\n";
			return false;
		}
		return true;
	}

	//does not support mipmapping or shadow maps
	bool createTextureSampler(VkDevice device, VkSampler &sampler)
	{
		VkSamplerCreateInfo samplerInfo;
		samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO; //structure type
		samplerInfo.magFilter = VK_FILTER_LINEAR; //specifies how to interpolate magnified images
		samplerInfo.minFilter = VK_FILTER_LINEAR; //specifies how to interpolate minified images
												  //addressing mode per axis
		samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT; //specifies to repeat the image beyond its size - x
		samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT; // - y
		samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT; // - z
		samplerInfo.anisotropyEnable = VK_TRUE; //turn anisotropic filtering on
		samplerInfo.maxAnisotropy = 16; //set to 16
		samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK; //set the border color
		samplerInfo.unnormalizedCoordinates = VK_FALSE; //use 0-1 to address coordinates
		samplerInfo.compareEnable = VK_FALSE; //set to off mainly used for shadows
		samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS; //test always passes
													  //by default library assumes that mipmappign is not active
		samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		samplerInfo.mipLodBias = 0.0f;
		samplerInfo.minLod = 0.0f;
		samplerInfo.maxLod = 0.0f;
		samplerInfo.flags = 0; //must be 0
		samplerInfo.pNext = VK_NULL_HANDLE;

		//create the sampler, returns false if unsuccessful
		if (vkCreateSampler(device, &samplerInfo, nullptr, &sampler) != VK_SUCCESS) {
			cout << "could not create a texture sampler\n";
			return false;
		}

		return true;
	}
}