#ifndef VKLibSHADER_H
#define VKLibSHADER_H

#include "vulkan.h"
#include "vk_sdk_platform.h"
#include "glm.hpp"
#include "spirv.h"
#include "GLSL.std.450.h"
#include "GlslangToSpv.h"
#include "ShaderLang.h"

#include "VKLibDefaults.h"

//for some reason wouldnt work otherwise
#include "../StandAlone/ResourceLimits.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

namespace VKLib 
{
	/*creates a shader stage and retures it. This function creates a shader module from spv code then genertates the shader stage, this stage should be used to create the pipeline later
	device - initialised vulkan device
	spv - stored spir-v code
	flagBits - shader stage flag bits
	shaderStage - the shader stage create info
	shaderModule - returns the created shaderModule
	returns false on failed creation
	*/
	bool createShaderStage(VkDevice device, vector<unsigned int> spv, VkShaderStageFlagBits flagBits, VkPipelineShaderStageCreateInfo *ShaderStage, VkShaderModule &shaderModule);
	
	/* loads a glsl shader from a file
	fname - file name of the shader
	returns the file contents in a char*
	*/
	char* loadGLSLShader(char* fName);

	/* takes in a fileName and determines the type of shader file. EG Example.vert
	fileName - the name of the file. Can include Path. Ensure that only one . is in name or path or functiuon wont work
	flag - returns the type of shader, used for creating shader modules at a later stage
	lang - returns the ESHLanguage of the shader, used to create SPV from GLSL code
	returns true if sucesful
	*/
	bool getShaderType(char* fileName, VkShaderStageFlagBits& flag, EShLanguage& lang);
	
	/* converts a GLSL shader to SPIR-V
	fName - file name of the GLSL shader 
	spv - returns the SPIR-V shader
	flagBits - shader stage flag bits 
	returns true on success
	*/
	bool convertGLSLShader(char* fName, vector<unsigned int>& spv, VkShaderStageFlagBits& flagBits);
	
	/*creates a graphics pipeline this created using a predifined system
	device - valid logic device
	stageInfo - valid shader stages stored in a vector
	vertexInput - valid vertex input data
	chainExtent - a valid chain extent
	renderPass - a valid render pass
	pipelineLayout - returns the pipeline layout
	returns the created graphics pipeline
	*/
	VkPipeline CreateGraphicsPipeline(VkDevice device, vector<VkPipelineShaderStageCreateInfo> stageInfo, VkPipelineVertexInputStateCreateInfo vertexInput, VkExtent2D chainExtent, VkRenderPass renderPass, VkPipelineLayout &pipelineLayout);
	
	/* creates a graphics pipeline this created using a predifined system
	device - valid logic device
	stageInfo - valid shader stages stored in a vector
	vertexInput - valid vertex input data
	chainExtent - a valid chain extent
	renderPass - a valid render pass
	descriptorlayout - valid descriptor layout for uniforms
	pipelineLayout - returns the pipeline layout
	returns the created graphics pipeline
	*/
	VkPipeline CreateGraphicsPipeline(VkDevice device, vector<VkPipelineShaderStageCreateInfo> stageInfo, VkPipelineVertexInputStateCreateInfo vertexInput, VkExtent2D chainExtent, VkRenderPass renderPass, VkDescriptorSetLayout descriptorlayout, VkPipelineLayout &pipelineLayout);
	
	/* creates a simple pipeline from a GLSL vert and fragment shader
	device - valid logic device
	vertFile - vertex shader file name
	fragFile - fragment shader file name
	renderPass - valid render pass
	chainExtent - valid chain extent
	vertexInput - valid vertex input data
	graphicsPipeline - returns the graphics pipeline
	pipelineLayout - returns the pipeline layout
	vertexShaderModule - returns the vertex shader module
	fragmentShaderModule - returns the fragment shader module
	returns true on successful creation
	*/
	bool createSimpleProgram(VkDevice device, char* vertFile, char* fragFile, VkRenderPass renderPass, VkExtent2D chainExtent, VkPipelineVertexInputStateCreateInfo vertexInput, VkPipeline& graphicsPipeline, VkPipelineLayout &pipelineLayout, VkShaderModule &vertexShaderModule, VkShaderModule &fragmentShaderModule);
	
	/* creates a simple pipeline from a GLSL vert and fragment shader
	device - valid logic device
	vertFile - vertex shader file name
	fragFile - fragment shader file name
	renderPass - valid render pass
	chainExtent - valid chain extent
	vertexInput - valid vertex input data
	descriptorLayout - valid descriptor layout for uniforms
	graphicsPipeline - returns the graphics pipeline
	pipelineLayout - returns the pipeline layout
	vertexShaderModule - returns the vertex shader module
	fragmentShaderModule - returns the fragment shader module
	returns true on successful creation
	*/
	bool createSimpleProgram(VkDevice device, char* vertFile, char* fragFile, VkRenderPass renderPass, VkExtent2D chainExtent, VkPipelineVertexInputStateCreateInfo vertexInput, VkDescriptorSetLayout descriptorLayout, VkPipeline& graphicsPipeline, VkPipelineLayout &pipelineLayout, VkShaderModule &vertexShaderModule, VkShaderModule &fragmentShaderModule);
}

#endif