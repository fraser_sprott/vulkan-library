#include "VKLibVertex.h"

namespace VKLib
{
	//few data types the user doesnt need to know about 
	struct VAO{
		VkBuffer vertexBuffer;
		VkBuffer indexBuffer;
		vector<vertex> vertices;
		vector<uint16_t> indices;
	};

	//will store vertex data in a map, this should help simplyfy the drawing and mesh creation, used for drawing specific objects
	static map<int, VAO> vertexArrayMap;

	int noOfVAO = -1;

	//return a basic vertex attribute description// this is needed for vertex in data
	vector<VkVertexInputAttributeDescription> getAttributeDescriptions()
	{
		//create a vector to store the data and set the size
		vector<VkVertexInputAttributeDescription> attributes = {};
		attributes.resize(4);

		//bind the position data
		attributes[0].binding = 0; //first bindiing slot
		attributes[0].location = 0; //the location
		attributes[0].format = VK_FORMAT_R32G32B32_SFLOAT; //specify the data type; this specifies a vec3
		attributes[0].offset = offsetof(vertex, position); 

		//bind the colour data
		attributes[1].binding = 0; //same binding slot
		attributes[1].location = 1; //the location
		attributes[1].format = VK_FORMAT_R32G32B32A32_SFLOAT; //specifies a vec4 
		attributes[1].offset = offsetof(vertex, color);

		//bind the texure coordinate data
		attributes[2].binding = 0; //same binding slot
		attributes[2].location = 2; //the location
		attributes[2].format = VK_FORMAT_R32G32_SFLOAT; //specifies a vec2
		attributes[2].offset = offsetof(vertex, texCoords);

		//bind the normals data
		attributes[3].binding = 0; //same binding slot
		attributes[3].location = 3; //the location
		attributes[3].format = VK_FORMAT_R32G32B32_SFLOAT; //specifies a vec2
		attributes[3].offset = offsetof(vertex, normals);

		//return the attribute description
		return attributes;
	}

	//returns the vertexInputBindingDescription with the size set to the vertex struct in the library
	vector<VkVertexInputBindingDescription> getBindingDescription() {
		vector<VkVertexInputBindingDescription> description; //binding structure
		description.resize(1);
		description[0].binding = 0; //since everything is in a struct set to zero
		description[0].stride = sizeof(vertex); //the size of the vertex, position and colour //requires more memory due to an extra coordinate
		description[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX; //decides when to move on, vertex specifies move after each vertex
		//returns the description
		return description;
	}

	VkPipelineVertexInputStateCreateInfo  getVertexInputInfo(vector<VkVertexInputBindingDescription> &vertexBinding, vector<VkVertexInputAttributeDescription> &attributes) {

		//set up the vertex input create info
		VkPipelineVertexInputStateCreateInfo vertexInput;
		vertexInput.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO; //type info
		vertexInput.vertexBindingDescriptionCount = vertexBinding.size(); //number of vertex bindings, 1 due to vertex data being a struct
		vertexInput.pVertexBindingDescriptions = vertexBinding.data();//add the binding description
		vertexInput.vertexAttributeDescriptionCount = attributes.size();//number of attributes
		vertexInput.pVertexAttributeDescriptions = attributes.data();//attributes descriptions //binding number, location and data type
		vertexInput.flags = 0;
		vertexInput.pNext = VK_NULL_HANDLE;
		//return the inputInfo
		return vertexInput;
	}

	//returns - 1 on unsuccessful creation
	//this function creates a buffer and stores the data inside the library, returns an integer for drawing
	int createMesh(VkDevice device, VkPhysicalDevice physicalDevice, vector<vertex> &vertices, vector<uint16_t> indices)
	{
		//set up a holder
		VAO vao;
		//add the vertices to the vao
		vao.vertices = vertices;
		vao.indices = indices;
		//set up data 
		VkDeviceSize size = sizeof(vertex) * vertices.size();

		//vertex memory
		VkDeviceMemory vertexMemory;
		//create the vertex buffer
		if (createBuffer(device, physicalDevice, size, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, 
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			vao.vertexBuffer, vertexMemory) == false)
		{
			return -1;
		}
		
		//map the memory
		void* data;
		vkMapMemory(device, vertexMemory, 0, size, 0, &data);
		memcpy(data, vertices.data(), size);
		vkUnmapMemory(device, vertexMemory);

		//set size to the indices size
		size = sizeof(indices[0]) * indices.size();
		//set the index memory
		VkDeviceMemory indexMemory;

		//create the index buffer
		if (createBuffer(device, physicalDevice, size, VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			vao.indexBuffer, indexMemory) == false)
		{
			return -1;
		}

		//map the memory
		data;
		vkMapMemory(device, indexMemory, 0, size, 0, &data);
		memcpy(data, indices.data(), size);
		vkUnmapMemory(device, indexMemory);

		//increase the number of vaos and create a handle to it
		noOfVAO++;
		int mesh = noOfVAO;
		//add the vao to the map
		vertexArrayMap.insert(pair<int, VAO>(mesh, vao));
		
		return mesh;
	}
	void drawMesh(VkCommandBuffer &commandBuffer, const int meshID)
	{
		//retrive the meshes vertex data
		VAO tmp = vertexArrayMap.at(meshID);
		VkDeviceSize offsets[] = { 0 };

		//bind the vertex buffer
		vkCmdBindVertexBuffers(commandBuffer, 0, 1, &tmp.vertexBuffer, offsets);
		
		//bind the index buffer
		vkCmdBindIndexBuffer(commandBuffer, tmp.indexBuffer, 0, VK_INDEX_TYPE_UINT16);

		//draw the vertex buffer
		vkCmdDrawIndexed(commandBuffer, tmp.indices.size(), 1, 0, 0, 0);
	}

	void drawMesh(VkCommandBuffer &commandBuffer, const int meshID, VkPipelineLayout pipelineLayout, vector<VkDescriptorSet> descriptorSet)
	{
		//retrive the meshes vertex data
		VAO tmp = vertexArrayMap.at(meshID);
		VkDeviceSize offsets[] = { 0 };

		//bind the vertex buffer
		vkCmdBindVertexBuffers(commandBuffer, 0, 1, &tmp.vertexBuffer, offsets);
		//bind the index buffer
		vkCmdBindIndexBuffer(commandBuffer, tmp.indexBuffer, 0, VK_INDEX_TYPE_UINT16);
		//bind the descriptor sets if any passed in
		vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, descriptorSet.size(), descriptorSet.data(), 0, nullptr);
		//draw the vertex buffer
		vkCmdDrawIndexed(commandBuffer, tmp.indices.size(), 1, 0, 0, 0);
	}
}