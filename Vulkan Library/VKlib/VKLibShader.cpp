#include "VKLibShader.h"

namespace VKLib
{
	//creates a shader stage and retures it. This function creates a shader module from spv code then genertates the shader stage
	//The stage should be used to create the pipeline later
	//device - initialised vulkan device
	//spv - stored spir-v code
	//
	//returns VK_NULL_HANDLE on failed creation
	bool createShaderStage(VkDevice device, vector<unsigned int> spv, VkShaderStageFlagBits flagBits, VkPipelineShaderStageCreateInfo *ShaderStage, VkShaderModule &shaderModule)
	{
		bool success = false;

		//setup the shader module info default for the moment
		VkShaderModuleCreateInfo shaderInfo;
		shaderInfo.codeSize = spv.size() * sizeof(unsigned int);
		shaderInfo.flags = 0;
		shaderInfo.pCode = spv.data();
		shaderInfo.pNext = VK_NULL_HANDLE;
		shaderInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;

		//create the shader module
		VkResult result = vkCreateShaderModule(device, &shaderInfo, VK_NULL_HANDLE, &shaderModule);

		switch (result)
		{
		case VK_SUCCESS:
			cout << "Shader Module Created" << endl;
			success =true;
			
			//create the shader stage info
			ShaderStage->sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
			ShaderStage->stage = flagBits;
			ShaderStage->module = shaderModule;
			ShaderStage->pName = "main";
			
			break;
		case VK_ERROR_OUT_OF_HOST_MEMORY:
			cout << "Could not create vertex shader module. out of host memory" << endl;
			break;
		case VK_ERROR_OUT_OF_DEVICE_MEMORY:
			cout << "Could not create vertex shader module. out of device memory" << endl;
			break;
		}
			

		return success;
	}

	char* loadGLSLShader(char* fName)
	{
		ifstream in(fName);

		//read the file using stringstream
		stringstream memblock;
		memblock << in.rdbuf();
		string buffer(memblock.str());

		buffer += '\0'; //add a VK_NULL_HANDLE terminator, stops random symbols loading
		const int size = buffer.size();

		//transfer the string to a char arrasy
		char* contents = new char[size];
		for (int i = 0; i < size; i++)
			contents[i] = buffer[i];

		//return the contents
		return contents;
	}

	//takes in a fileName and determines the type of shader file. EG Example.vert
	//char* fileName - the name of the file. Can include Path. Ensure that only one . is in name or path or functiuon wont work
	//VkShaderStageFlagBits* flag - returns the type of shader, used for creating shader modules at a later stage
	//EShLanguage* lang - returns the ESHLanguage of the shader, used to create SPV from GLSL code
	bool getShaderType(char* fileName, VkShaderStageFlagBits& flag, EShLanguage& lang)
	{
		//ensure that fileName is not null
		if (fileName == NULL)
			return false;

		string type;
		int i = 0;
		//cycle through the file name until it reaches the . or there is a gap
		while (fileName[i] != '.' || fileName[i] == ' ')
		{
			i++;//next char
		}

		//add 1 to i to move off the '.' then loop through the next four adding it to type
		i++;
		while (type.length() < 4 || fileName[i] == ' ')
		{
			type += fileName[i];
			i++;
		}

		//true if file type is recognised
		bool found = false;
		//search to see if what file type was there
		if (type == "vert")
		{
			flag = VK_SHADER_STAGE_VERTEX_BIT;
			lang = EShLangVertex;
			found = true;
		}
		else if (type == "tesc")
		{
			flag = VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
			lang = EShLangTessControl;
			found = true;
		}
		else if (type == "tese")
		{
			flag = VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
			lang = EShLangTessEvaluation;
			found = true;
		}
		else if (type == "geom")
		{
			flag = VK_SHADER_STAGE_GEOMETRY_BIT;
			lang = EShLangGeometry;
			found = true;
		}
		else if (type == "frag")
		{
			flag = VK_SHADER_STAGE_FRAGMENT_BIT;
			lang = EShLangFragment;
			found = true;
		}
		else if (type == "comp")
		{
			flag = VK_SHADER_STAGE_COMPUTE_BIT;
			lang = EShLangCompute;
			found = true;
		}

		//return false in it file type was not found
		return found;
	}

	bool convertGLSLShader(char* fName, vector<unsigned int>& spv, VkShaderStageFlagBits& flagBits)
	{
		EShLanguage shaderType;
		bool pass = getShaderType(fName, flagBits, shaderType);

		//don't continue if pass is false as shader type cannot be found
		if (pass == true) {
			//load the file contents
			char* shader = loadGLSLShader(fName);

			//continue if file loaded successfully
			if (pass == true)
			{
				glslang::TShader t_shader(shaderType);
				glslang::TProgram program;

				//Enable SPIR - V and Vulkan rules when parsing GLSL
				EShMessages messages = (EShMessages)(EShMsgSpvRules | EShMsgVulkanRules);

				//use the default resources provided by the glslanf standalone //later versions will include user defined resources
				TBuiltInResource resources = glslang::DefaultTBuiltInResource;

				const char *shaderStrings[1];
				shaderStrings[0] = shader;
				t_shader.setStrings(shaderStrings, 1);
				glslang::InitializeProcess();
				

				if (!t_shader.parse(&resources, 450, false, messages)) {
					puts(t_shader.getInfoLog());
					puts(t_shader.getInfoDebugLog());
					return false; // something didn't work
				}

				program.addShader(&t_shader);

				if (!program.link(messages)) {
					puts(t_shader.getInfoLog());
					puts(t_shader.getInfoDebugLog());
					fflush(stdout);
					return false;
				}
				
				//takes in the shader code and creates a spv shader using the glsang interface
				glslang::GlslangToSpv(*program.getIntermediate(shaderType), spv);

				glslang::FinalizeProcess();
			}
			else {
				cout << "Could not convert " << fName << "could not load the file." << endl;
				return false;
			}
		}
		else {
			cout << "Could not convert " << fName << "could not determine the shader type." << endl;
			return false;
		}
		return true;
	}
	//creates and returns a single graphic pipeline
	//this only takes in the needed parameters for creation and will create default parameters of nonessential aspects 
	VkPipeline CreateGraphicsPipeline(VkDevice device, vector<VkPipelineShaderStageCreateInfo> stageInfo, VkPipelineVertexInputStateCreateInfo vertexInput, VkExtent2D chainExtent, VkRenderPass renderPass, VkPipelineLayout &pipelineLayout)
	{
		VkGraphicsPipelineCreateInfo pipelineCreateInfo;
		pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO; //sets up the structure type
		pipelineCreateInfo.pVertexInputState = &vertexInput;
		pipelineCreateInfo.stageCount = stageInfo.size(); //the size of the vector
		pipelineCreateInfo.pStages = stageInfo.data(); //the shaders themself		

		//get and add the input assembly to the create Info
		VkPipelineInputAssemblyStateCreateInfo inputAssembly = VKLibDefaults::getDefaultInputAssemblyInfo();
		pipelineCreateInfo.pInputAssemblyState = &inputAssembly;

		//no default pTessellationState
		pipelineCreateInfo.pTessellationState = nullptr;

		VkViewport viewport = VKLibDefaults::getDefaultViewport(chainExtent);
		VkRect2D scissor = VKLibDefaults::getDefaultScissor(chainExtent);

		//if viewportStateInfo has not been predefined, define it
		VkPipelineViewportStateCreateInfo viewportStateInfo = VKLibDefaults::getDefaultViewportStateInfo(viewport, scissor);

		//add the viewportstate assembly info to the pipeline create info
		pipelineCreateInfo.pViewportState = &viewportStateInfo;

		//if rasterization has not been predefined, define it
		VkPipelineRasterizationStateCreateInfo rasterizationInfo = VKLibDefaults::getDefaultRasterizationStateInfo();
		//add the raterization info to the pipeline create info
		pipelineCreateInfo.pRasterizationState = &rasterizationInfo;

		//if multisampling has not been predefined, define it
		VkPipelineMultisampleStateCreateInfo multisamplingInfo = VKLibDefaults::getDefaultMultisampleStageInfo();
		//add multisampling 
		pipelineCreateInfo.pMultisampleState = &multisamplingInfo;

		//depthStencil should be defined outside this class as it requires buffers and a few other things to work
		//add it to the pipeline create info
		pipelineCreateInfo.pDepthStencilState = nullptr;

		//if colour blending has not been predefined, define it
		//enables colour blending
		VkPipelineColorBlendAttachmentState colorBlendAtach = VKLibDefaults::getDefaultColorBlendAttachment();
		VkPipelineColorBlendStateCreateInfo colorBlend = VKLibDefaults::getDefaultColorBlendInfo(colorBlendAtach);
		//add the colorBlend to the pipeline create info
		pipelineCreateInfo.pColorBlendState = &colorBlend;

		//dynamic states allow the user to modify certain components of the pipeline after its been created such as linewidth, viewport 
		//idefine the dynamicstates
		VkPipelineDynamicStateCreateInfo dynamicState = VKLibDefaults::getDefaultDynamicStateInfo();
		//add the dynamicState to the pipeline create info
		pipelineCreateInfo.pDynamicState = &dynamicState;

		//define the pipeline create info
		VkPipelineLayoutCreateInfo layoutInfo = VKLibDefaults::getDefaultLayoutInfo();
		//create the pipeline layout and add it to the pipline create info
		vkCreatePipelineLayout(device, &layoutInfo, nullptr, &pipelineLayout);
		pipelineCreateInfo.layout = pipelineLayout;

		//pass the other paramaters into the create info
		pipelineCreateInfo.renderPass = renderPass;
		pipelineCreateInfo.subpass = 0;
		pipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
		pipelineCreateInfo.basePipelineIndex = -1; // Optional
		pipelineCreateInfo.pNext = VK_NULL_HANDLE;
		pipelineCreateInfo.flags = 0;

		//create the graphics pipeline
		VkPipeline pipeline;
		vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipelineCreateInfo, nullptr, &pipeline);

		return pipeline;
	}

	//creates and returns a single graphic pipeline
	//this only takes in the needed parameters for creation and will create default parameters of nonessential aspects 
	VkPipeline CreateGraphicsPipeline(VkDevice device, vector<VkPipelineShaderStageCreateInfo> stageInfo, VkPipelineVertexInputStateCreateInfo vertexInput, VkExtent2D chainExtent, VkRenderPass renderPass, VkDescriptorSetLayout descriptorlayout, VkPipelineLayout &pipelineLayout)
	{
		VkGraphicsPipelineCreateInfo pipelineCreateInfo;
		pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO; //sets up the structure type
		pipelineCreateInfo.pVertexInputState = &vertexInput;
		pipelineCreateInfo.stageCount = stageInfo.size(); //the size of the vector
		pipelineCreateInfo.pStages = stageInfo.data(); //the shaders themself		

													   //get and add the input assembly to the create Info
		VkPipelineInputAssemblyStateCreateInfo inputAssembly = VKLibDefaults::getDefaultInputAssemblyInfo();
		pipelineCreateInfo.pInputAssemblyState = &inputAssembly;

		//no default pTessellationState
		pipelineCreateInfo.pTessellationState = nullptr;

		VkViewport viewport = VKLibDefaults::getDefaultViewport(chainExtent);
		VkRect2D scissor = VKLibDefaults::getDefaultScissor(chainExtent);

		//if viewportStateInfo has not been predefined, define it
		VkPipelineViewportStateCreateInfo viewportStateInfo = VKLibDefaults::getDefaultViewportStateInfo(viewport, scissor);

		//add the viewportstate assembly info to the pipeline create info
		pipelineCreateInfo.pViewportState = &viewportStateInfo;

		//if rasterization has not been predefined, define it
		VkPipelineRasterizationStateCreateInfo rasterizationInfo = VKLibDefaults::getDefaultRasterizationStateInfo();
		//add the raterization info to the pipeline create info
		pipelineCreateInfo.pRasterizationState = &rasterizationInfo;

		//if multisampling has not been predefined, define it
		VkPipelineMultisampleStateCreateInfo multisamplingInfo = VKLibDefaults::getDefaultMultisampleStageInfo();
		//add multisampling 
		pipelineCreateInfo.pMultisampleState = &multisamplingInfo;

		//depthStencil should be defined outside this class as it requires buffers and a few other things to work
		//add it to the pipeline create info
		pipelineCreateInfo.pDepthStencilState = nullptr;

		//if colour blending has not been predefined, define it
		//enables colour blending
		VkPipelineColorBlendAttachmentState colorBlendAtach = VKLibDefaults::getDefaultColorBlendAttachment();
		VkPipelineColorBlendStateCreateInfo colorBlend = VKLibDefaults::getDefaultColorBlendInfo(colorBlendAtach);
		//add the colorBlend to the pipeline create info
		pipelineCreateInfo.pColorBlendState = &colorBlend;

		//dynamic states allow the user to modify certain components of the pipeline after its been created such as linewidth, viewport 
		//idefine the dynamicstates
		VkPipelineDynamicStateCreateInfo dynamicState = VKLibDefaults::getDefaultDynamicStateInfo();
		//add the dynamicState to the pipeline create info
		pipelineCreateInfo.pDynamicState = &dynamicState;

		//create the layout info
		VkPipelineLayoutCreateInfo layoutInfo;
		layoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		layoutInfo.setLayoutCount = 1;
		layoutInfo.pSetLayouts = &descriptorlayout;
		layoutInfo.flags = 0;
		layoutInfo.pushConstantRangeCount = 0; //number of push constant ranges included 
		layoutInfo.pPushConstantRanges = 0; //the push constant ranges
		layoutInfo.pNext = VK_NULL_HANDLE;

		//create the pipeline layout and add it to the pipline create info
		vkCreatePipelineLayout(device, &layoutInfo, nullptr, &pipelineLayout);
		pipelineCreateInfo.layout = pipelineLayout;

		//pass the other paramaters into the create info
		pipelineCreateInfo.renderPass = renderPass;
		pipelineCreateInfo.subpass = 0;
		pipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
		pipelineCreateInfo.basePipelineIndex = -1; // Optional
		pipelineCreateInfo.pNext = VK_NULL_HANDLE;
		pipelineCreateInfo.flags = 0;

		//create the graphics pipeline
		VkPipeline pipeline;
		vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipelineCreateInfo, nullptr, &pipeline);

		return pipeline;
	}

	//creates a simple pipeline from a GLSL vert and fragment shader
	//this needs a pipeline info to have been created leaving out the shader stage info as this function creates the shaders
	bool createSimpleProgram(VkDevice device, char* vertFile, char* fragFile, VkRenderPass renderPass, VkExtent2D chainExtent, VkPipelineVertexInputStateCreateInfo vertexInput, VkPipeline& graphicsPipeline, VkPipelineLayout &pipelineLayout, VkShaderModule &vertexShaderModule, VkShaderModule &fragmentShaderModule)
	{
		//only returns true on successful creation
		bool successful = true;

		//vert spv holders
		vector<unsigned int> vertSPV = {};
		VkShaderStageFlagBits vertFlag;

		//convert the vert shader to spv
		bool vertPass = convertGLSLShader(vertFile, vertSPV, vertFlag);

		//do the same with the frag shader
		//frag spv holders
		vector<unsigned int> fragSPV = {};
		VkShaderStageFlagBits fragFlag;

		//convert the frag shader to spv
		bool fragPass = convertGLSLShader(fragFile, fragSPV, fragFlag);

		//if either failed to convert dont continue
		if (vertPass == true && fragPass == true)
		{
			vector<VkPipelineShaderStageCreateInfo> stageInfo;
			stageInfo.resize(2);
			//reuse the vert and frag pass booleans
			vertPass = createShaderStage(device, vertSPV, VK_SHADER_STAGE_VERTEX_BIT, &stageInfo[0], vertexShaderModule);

			//create the shader module
			//VkShaderModule fragShaderModule;
			fragPass = createShaderStage(device, fragSPV, VK_SHADER_STAGE_FRAGMENT_BIT, &stageInfo[1], fragmentShaderModule);

			//check again for any failures
			if (fragPass == true && vertPass == true && renderPass != VK_NULL_HANDLE)
			{
				//create the pipeline
				graphicsPipeline = CreateGraphicsPipeline(device, stageInfo, vertexInput, chainExtent, renderPass, pipelineLayout);
			}
			else
			{
				//print usefull information regarding the unsucessful creation
				cout << "Could Not Create Shader Modules. Shader Created: Fragment: " << fragPass << ",  Vert Shader: " << vertPass << endl;
				successful = false;
			}
		}
		else
		{
			//print usefull information regarding the unsucessful creation
			cout << "Could Not convert Shaders: \n	Shader converted: Fragment: " << fragPass << " \n	Vert Shader: " << vertPass << endl;
			successful = false;
		}

		return successful;
	}

	//creates a simple pipeline from a GLSL vert and fragment shader
	//this needs a pipeline info to have been created leaving out the shader stage info as this function creates the shaders
	//returns the layout and the pipeline
	bool createSimpleProgram(VkDevice device, char* vertFile, char* fragFile, VkRenderPass renderPass, VkExtent2D chainExtent, VkPipelineVertexInputStateCreateInfo vertexInput, VkDescriptorSetLayout descriptorLayout, VkPipeline& graphicsPipeline, VkPipelineLayout &pipelineLayout, VkShaderModule &vertexShaderModule, VkShaderModule &fragmentShaderModule)
	{
		//only returns true on successful creation
		bool successful = true;

		//vert spv holders
		vector<unsigned int> vertSPV = {};
		VkShaderStageFlagBits vertFlag;

		//convert the vert shader to spv
		bool vertPass = convertGLSLShader(vertFile, vertSPV, vertFlag);

		//do the same with the frag shader
		//frag spv holders
		vector<unsigned int> fragSPV = {};
		VkShaderStageFlagBits fragFlag;

		//convert the frag shader to spv
		bool fragPass = convertGLSLShader(fragFile, fragSPV, fragFlag);

		//if either failed to convert dont continue
		if (vertPass == true && fragPass == true)
		{
			vector<VkPipelineShaderStageCreateInfo> stageInfo;
			stageInfo.resize(2);
			//reuse the vert and frag pass booleans
			vertPass = createShaderStage(device, vertSPV, VK_SHADER_STAGE_VERTEX_BIT, &stageInfo[0], vertexShaderModule);

			//create the shader module
			//VkShaderModule fragShaderModule;
			fragPass = createShaderStage(device, fragSPV, VK_SHADER_STAGE_FRAGMENT_BIT, &stageInfo[1], fragmentShaderModule);

			//check again for any failures
			if (fragPass == true && vertPass == true && renderPass != VK_NULL_HANDLE)
			{
				//create the pipeline
				graphicsPipeline = CreateGraphicsPipeline(device, stageInfo, vertexInput, chainExtent, renderPass, descriptorLayout, pipelineLayout);
			}
			else
			{
				//print usefull information regarding the unsucessful creation
				cout << "Could Not Create Shader Modules. Shader Created: Fragment: " << fragPass << ",  Vert Shader: " << vertPass << endl;
				successful = false;
			}
		}
		else
		{
			//print usefull information regarding the unsucessful creation
			cout << "Could Not convert Shaders: \n	Shader converted: Fragment: " << fragPass << " \n	Vert Shader: " << vertPass << endl;
			successful = false;
		}

		return successful;
	}
}