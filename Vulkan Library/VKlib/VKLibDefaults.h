#ifndef VKLIBDEFAULTS_H
#define VKLIBDEFAULTS_H

#include "vulkan.h"
#include "vk_sdk_platform.h"

namespace VKLibDefaults {
	//returns a predefined vertexInput
	VkPipelineVertexInputStateCreateInfo getDefaultVertexInputInfo();
	
	//returns a predefined vertexInput
	VkPipelineInputAssemblyStateCreateInfo getDefaultInputAssemblyInfo();
	
	/*get the defualt viewport
	chainExtent - the swapchains extent
	returns the viewport
	*/
	VkViewport getDefaultViewport(VkExtent2D chainExtent);
	
	/*get the defualt scissor
	chainExtent - the swapchains extent
	returns the scissor
	*/
	VkRect2D getDefaultScissor(VkExtent2D chainExtent);
	
	/*get the defualt viewport state info for the pipeline creation
	viewport - the viewport
	scissor - the scissor
	returns the viewport state create info 
	*/
	VkPipelineViewportStateCreateInfo getDefaultViewportStateInfo(VkViewport viewport,VkRect2D scissor);
	
	//returns a predefined rasterizationStateInfo
	VkPipelineRasterizationStateCreateInfo getDefaultRasterizationStateInfo();
	
	//returns a predefined multisampleStateinfo
	VkPipelineMultisampleStateCreateInfo getDefaultMultisampleStageInfo();
	
	//returns a predefined color blendattatchment state 
	VkPipelineColorBlendAttachmentState getDefaultColorBlendAttachment();
	
	/*creates a default color blend info
	colorBlend - a valid color blend attachment state
	returns a predefined colorBlendStateInfo
	*/
	VkPipelineColorBlendStateCreateInfo getDefaultColorBlendInfo(VkPipelineColorBlendAttachmentState colorBlend);
	
	//returns a predefined dynamicStateInfo
	VkPipelineDynamicStateCreateInfo getDefaultDynamicStateInfo();
	
	//returns a predefined layoutCreateInfo
	VkPipelineLayoutCreateInfo getDefaultLayoutInfo();
}

#endif
