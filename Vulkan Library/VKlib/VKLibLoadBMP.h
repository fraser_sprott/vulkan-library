#ifndef VKLIBLOADBMP_H
#define VKLIBLOADBMP_H

/* this file contains the code for loading bmp images through the stb image library
it also provides functions that support textures in a vulkan applications 
including setup */

#include "VKLib.h"

namespace VKLib
{
	/*creates the vk image
	physicalDevice - physical device
	device - logic device
	width - image width
	height - image height
	format - valid VkFormat
	tiling - image tilling
	usage - valide image usage flags
	properties - memory properties
	image - creates and returns the image
	imageMemory - creates and returns the image memory
	returns true on valid creation
	*/
	bool createImage(VkPhysicalDevice physicalDevice, VkDevice device, int width, int height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage &image, VkDeviceMemory &imageMemory);

	/* takes a bmp image in and loads it in from the file specified
	physicalDevice - physical device
 	device - logic device
	filename - the file name
	image - creates and returns an imageStage struct
	format - valid VkFormat
	returns true on success
	*/	
	bool loadTexture(VkPhysicalDevice physicalDevice, VkDevice device, char* filename, imageStages &image, VkFormat format);

	/* used for setting up the image for a commandBuffer
	commandBuffer - the command buffer
	image - the image
	format - valid VkFormat
	oldLayout - the old layout to replace
	newLayout - the new layout to add
	*/
	void transitionImageLayout(VkCommandBuffer &commandBuffer, VkImage &image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout);

	/*copies the staging image to the command buffer
	VkCommandBuffer - the valid command buffer
	image - valid image stages
	*/
	void copyImage(VkCommandBuffer &commandBuffer, imageStages &image);

	/* used by the  prepareTextureImage this function sets up a command buffer to run once as copy image shouldnt be used during a renderPass 
	device - logic device
	commandBuffer - the commandbuffer to start recording with
	commandPool - the command pool
	*/
	void startCommand(VkDevice device, VkCommandBuffer &commandBuffer, VkCommandPool &commandPool);

	/* used by the  prepareTextureImage this function ends a command buffer that is only runnign once as copy image should not be used during a renderPass 
	device - logic device
	commandBuffer - the command buffer to stop recording
	graphicsQueue - the graphics queue
	commandPool - the command pool
	*/
	void endCommand(VkDevice device, VkCommandBuffer &commandBuffer, VkQueue &graphicsQueue, VkCommandPool &commandPool);

	/*this function prepares the texture so that the command buffers can transfer the staging image to the texture
	device - logic device
	commandPool - the command pool 
	graphicsQueue - the graphics queue
	image - valid image stages struct
	*/
	void prepareTextureImage(VkDevice device, VkCommandPool commandPool, VkQueue graphicsQueue, imageStages image);

	/*creates the image view
	device - logic device
	imageView - returns the image view 
	image - valid image
	format - valid VkFormat
	returns true on successful creation
	*/
	bool createImageView(VkDevice device, VkImageView &imageView, VkImage &image, VkFormat format);

	/*creates the texture sampler
	note this does not support mipmapping or shadow maps
	device - logic device
	sampler - returns the created sampler
	returns true on successful creation
	*/
	bool createTextureSampler(VkDevice device, VkSampler &sampler);
}

#endif