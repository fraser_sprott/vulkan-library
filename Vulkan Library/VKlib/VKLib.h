#ifndef VKLib_H
#define VKLib_H

#include "vulkan.h"
#include "vk_sdk_platform.h"
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include "glm.hpp"
#include "spirv.h"
#include "GLSL.std.450.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std; 

namespace VKLib
{
	/* light structure
	ambiet - the ambient colour of the light
	diffuse - the diffuse colour of the light
	specular - the specular colour of the light
	position - the position of the light */
	struct lightStruct {
		float ambient[4];
		float diffuse[4];
		float specular[4];
		float position[4];
	};

	/* material structure
	ambiet - the ambient colour of the light
	diffuse - the diffuse colour of the light
	specular - the specular colour of the light
	position - the position of the light */
	struct materialStruct {
		float ambient[4];
		float diffuse[4];
		float specular[4];
		float shininess;
	};

	/* view structure used to store projection and modelview
	projection - projection - orthoganol or projection
	modelView - the modelView is the object transformations in camera space
	cameraView - the camera view matrix
	cameraPos - the position of the camera */
	struct viewStruct {
		glm::mat4 projection;
		glm::mat4 modelView;
		glm::mat4 cameraView;
		glm::vec3 cameraPos;
	};

	/* due to the chance that image is only suitable as a staging image 
	a second texture will be used for the device
	staging image - the cpu image
	texture - the texture that is used for the device
	stagingMemory - the memory of the image on cpu
	textureMemory - the memory of the image on device
	*/
	struct imageStages {
		VkImage stagingImage;
		VkImage texture;
		VkDeviceMemory stagingMemory;
		VkDeviceMemory textureMemory;
		VkExtent2D dimension;
	};
	
	/* stores information regarding queues that meet the requirements of the app
		graphicFam - stores the number of graphic queue families
		presentFam - stores the number of present queues families
		isSuitable - returns true if there is suitable queues 
		*/
	struct graphicFamilyindex
	{
		//if -1 no queues available
		int graphicFam = -1;
		int presentFam = -1;
		//return true if there is a suitableQueue and presentFamily
		bool isSuitable() { return graphicFam >= 0 && presentFam >= 0; }
	};

	/*handy stucture to keep track of the varioius components of the swap chain 
	surfaceCapabilities - stores the surface capability
	surfaceFormats - stores the surface formats in a vector
	presentModes - stores the present mode in a vector	
	*/
	struct SwapChainSupport {
		VkSurfaceCapabilitiesKHR surfaceCapabilities;
		vector<VkSurfaceFormatKHR> surfaceFormats;
		vector<VkPresentModeKHR> presentModes;
	};

	/*exits the program 
	param message - message to display to the console
	*/
	void exitFatalError(const char *message);

	/* loads a file
	fname - the file name
	fSize - gets the files size and returns the value	
	returns the file contents as a char*
	*/
	char* loadFile(const char *fname, int &fSize);
		
	/*finds a suitable memory type
	filter - memory requirement flag bits
	propertyFlags - property flags
	device - Physical device
	returns the number of suitable memory, returns -1 on failure
	*/
	int findSuitableMemoryType(int filter, VkMemoryPropertyFlags propertyFlags, VkPhysicalDevice device);
	
	/*creates a buffer, returns true on successfull creation
	device - the logic device
	physicalDevice - physical device
	size - buffer size
	bufferFlags - buffer flags
	memoryProperties - memory properties
	buffer - returns the created buffer
	memory - returns the created buffer memory
	returns true on successfull
	*/
	bool createBuffer(VkDevice device, VkPhysicalDevice physicalDevice, VkDeviceSize size, VkBufferUsageFlags bufferFlags, VkMemoryPropertyFlags memoryProperties, VkBuffer &buffer, VkDeviceMemory &memory);
	
	/*creates a buffer for the viewStruct 
	device - the logic device
	physicalDevice - physical device
	buffer - returns the created buffer
	memory - returns the created buffer memory
	*/
	void createUniformViewBuffer(VkDevice device, VkPhysicalDevice physicalDevice, VkBuffer &buffer, VkDeviceMemory &bufferMemory);
	
	/*updates the uniform viewStruct buffer
	device - the logic device
	bufferMemory - updates the buffers memory
	viewStrut - the view structure to update the buffer with
	*/
	void updateViewUniformBuffer(VkDevice device, VkDeviceMemory &bufferMemory, viewStruct &view);

	/*creates a buffer for the materialStruct 
	device - the logic device
	physicalDevice - physical device
	buffer - returns the created buffer
	memory - returns the created buffer memory
	*/ 
	void createUniformMaterialBuffer(VkDevice device, VkPhysicalDevice physicalDevice, VkBuffer &buffer, VkDeviceMemory &bufferMemory);
	
	/*updates the uniform materialStruct buffer
	device - the logic device
	bufferMemory - updates the buffers memory
	material - the material structure to update the buffer
	*/
	void updateMaterialUniformBuffer(VkDevice device, VkDeviceMemory &bufferMemory, materialStruct &material);

	/*creates a buffer for the lightStruct 
	device - the logic device
	physicalDevice - physical device
	buffer - returns the created buffer
	memory - returns the created buffer memory
	*/
	void createUniformLightBuffer(VkDevice device, VkPhysicalDevice physicalDevice, VkBuffer &buffer, VkDeviceMemory &bufferMemory);
	
	/*updates the uniform lightStruct buffer
	device - the logic device
	bufferMemory - updates the buffers memory
	material - the light structure to update the buffer
	*/
	void updateLightUniformBuffer(VkDevice device, VkDeviceMemory &bufferMemory, lightStruct &light);

	/*creates the descriptor pool
	device - logic device
	poolSize - vector of descriptor pool size
	maxSets - maximim number of sets
	descriptorPool - returnms the created descriptor pool
	returns true on successful creation
	*/
	bool createDescriptorPool(VkDevice device, vector<VkDescriptorPoolSize> poolSize, int maxSets, VkDescriptorPool &descPool);
	
	/*creates a descriptor set
	device - logic device
	descPool - descriptor pool
	layouts - vector of descriptor set layouts
	descriptorSetCount - descriptor set count
	descriptorSet - returns the descriptor set
	rerurns true on successful creation
	*/
	bool createDescriptorSet(VkDevice device, VkDescriptorPool descPool, vector<VkDescriptorSetLayout> layouts, int descriptorSetCount, VkDescriptorSet &descriptorSet);

	/*updates the uniform descriptor set
	device - logic device
	bufferInfo - takes a valid descriptor buffer info
	descriptorSet - descriptor set
	binding - binding number
	arraryElememt - array element
	descriptorCount - descriptor count
	*/
	void updateUniformDescriptorSets(VkDevice device, VkDescriptorBufferInfo &bufferInfo, VkDescriptorSet descriptorSet, int binding, int arraryElememt, int descriptorCount);
	
	/*update the image descriptor sets
	device - logic device
	imageView - image view
	image sampler - sampler
	binding - binding number
	arraryElememt - array element
	descriptorCount - descriptor count
	*/
	void updateImageDescriptorSets(VkDevice device, VkImageView &imageView, VkSampler imageSampler, VkDescriptorSet descriptorSet, int binding, int arraryElememt, int descriptorCount);
	
	/*creates the descriptor set layout binding
	binding - the binding location
	descriptorType - type of descriptor
	stageFlags - shader stage flags bits
	returns the descriptor set layout binding
	*/
	VkDescriptorSetLayoutBinding createDescriptorSetLayoutBinding(int binding, VkDescriptorType type, VkShaderStageFlagBits stageFlags);
	 
	/*create the descriptor set layout
	device - logic device
	layout - descriptor set layout
	layoutBindings - vector of descriptor set layout  bindings
	returns true on successful creation
	*/
	bool createDescriptorSetLayout(VkDevice device, VkDescriptorSetLayout &layout, vector<VkDescriptorSetLayoutBinding> layoutBindings);

	/*queries the level of swap chain support
	device - physical device
	surface - a valid surface
	returns a swapChainSupport struct
	*/
	SwapChainSupport querySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface);

	/*gets the graphics family
	device - physical device
	surface - a valid surface
	returns a graphicFamilyindex struct
	*/
	graphicFamilyindex getgraphicFamilies(VkPhysicalDevice device, VkSurfaceKHR surface);
}

#endif