#include "VKLib.h"

namespace VKLib
{
	//display message then exit
	void exitFatalError(const char *message){
		//print error message
		cout << message << endl;
		exit(0);
	}

	//opens a file and returns the contents in a character array
	//remember to delete array after use to avoid memory leaks
	//used for the loading of GLSL shaders
	char* loadFile(const char *fname, int &fSize){
		ifstream file(fname, ios::ate | ios::binary);
		char* fileContents;
		//check if the file is open and if not print message
		if (file.is_open() == false) {
			cout << "Could not open file " << fname << endl;
		}
		else //file is open
		{
			//get and store the file size 
			int size = (int)file.tellg(); 
			fSize = (int)size;

			//read file to char array from beggining and close file
			fileContents = new char[size];
			file.seekg(0, ios::beg);
			file.read(fileContents, size);
			file.close();

			//print completion message
			cout << "file " << fname << " loaded" << endl;
		}

		return fileContents;
	}

	int findSuitableMemoryType(int filter, VkMemoryPropertyFlags propertyFlags, VkPhysicalDevice device) {
		//device memory properties handle
		VkPhysicalDeviceMemoryProperties properties;
		//gets the propeties of the devices memory
		vkGetPhysicalDeviceMemoryProperties(device, &properties);

		//find a suitable memory type for the vertex buffer
		for (int i = 0; i < properties.memoryTypeCount; i++)
		{
			//filter is used to specify suitable bit field memory types 
			if (filter & (1 << i) && (properties.memoryTypes[i].propertyFlags & propertyFlags) == propertyFlags)
			{
				return i;
			}
		}
		return -1;//failed to get a suitable memory type
	}
	
	bool createBuffer(VkDevice device, VkPhysicalDevice physicalDevice, VkDeviceSize size, VkBufferUsageFlags bufferFlags, VkMemoryPropertyFlags memoryProperties, VkBuffer &buffer, VkDeviceMemory &memory)
	{
		//create the buffer info
		VkBufferCreateInfo bufInfo = {};
		bufInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO; //info type
		bufInfo.size = size; //what is the size of the vertex data size of 1 * number
		bufInfo.usage = bufferFlags; //specifies how the data is to be used
		bufInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE; //will only be used by the draw queue

		//create the buffer
		VkResult res = vkCreateBuffer(device, &bufInfo, nullptr, &buffer);
		if (res != VK_SUCCESS)
		{
			cout << "Could not create vertex Buffer" << endl;
			return false;
		}

		//buffer needs memmory assigned to it
		VkMemoryRequirements req;
		vkGetBufferMemoryRequirements(device, buffer, &req);

		int typeIndex = findSuitableMemoryType(req.memoryTypeBits, memoryProperties, physicalDevice);

		//check that typeIndex was suitable
		if (typeIndex == -1)
		{
			cout << "Could not find a suitable memory type for the vertex buffer" << endl;
		}

		//finally allocate the memory to the buffer
		VkMemoryAllocateInfo  allInfo = {};
		allInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO; //Specify the type
		allInfo.allocationSize = req.size;//the required size
		allInfo.memoryTypeIndex = typeIndex; //memory type index
	
		//allocate the memory
		vkAllocateMemory(device, &allInfo, nullptr, &memory);

		//bind the buffer to the memory
		res = vkBindBufferMemory(device, buffer, memory, 0);
		if (res != VK_SUCCESS)
		{ 
			cout << "could not bind buffer memory\n";
			return false;
		}
		return true;
	}

	void createUniformViewBuffer(VkDevice device, VkPhysicalDevice physicalDevice, VkBuffer &buffer, VkDeviceMemory &bufferMemory)
	{
		//buffer size
		VkDeviceSize bufferSize = sizeof(viewStruct);

		//create the uniformBuffer
		createBuffer(device, physicalDevice, bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, 
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 
			buffer, bufferMemory);
	}

	//updates the uniform buffer
	void updateViewUniformBuffer(VkDevice device, VkDeviceMemory &bufferMemory, viewStruct &view)
	{
		void* data;
		vkMapMemory(device, bufferMemory, 0, sizeof(view), 0, &data);
		memcpy(data, &view, sizeof(view));
		vkUnmapMemory(device, bufferMemory);
	}

	//creates the material
	void createUniformMaterialBuffer(VkDevice device, VkPhysicalDevice physicalDevice, VkBuffer &buffer, VkDeviceMemory &bufferMemory)
	{
		//buffer size
		VkDeviceSize bufferSize = sizeof(materialStruct);

		//create the uniformBuffer
		createBuffer(device, physicalDevice, bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			buffer, bufferMemory);
	}

	//updates the uniform buffer
	void updateMaterialUniformBuffer(VkDevice device, VkDeviceMemory &bufferMemory, materialStruct &material)
	{
		void* data;
		vkMapMemory(device, bufferMemory, 0, sizeof(material), 0, &data);
		memcpy(data, &material, sizeof(material));
		vkUnmapMemory(device, bufferMemory);
	}

	//creates the light Buffer
	void createUniformLightBuffer(VkDevice device, VkPhysicalDevice physicalDevice, VkBuffer &buffer, VkDeviceMemory &bufferMemory)
	{
		//buffer size
		VkDeviceSize bufferSize = sizeof(lightStruct);

		//create the uniformBuffer
		createBuffer(device, physicalDevice, bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			buffer, bufferMemory);
	}

	//updates the uniform buffer
	void updateLightUniformBuffer(VkDevice device, VkDeviceMemory &bufferMemory, lightStruct &light)
	{
		void* data;
		vkMapMemory(device, bufferMemory, 0, sizeof(light), 0, &data);
		memcpy(data, &light, sizeof(light));
		vkUnmapMemory(device, bufferMemory);
	}

	bool createDescriptorPool(VkDevice device, vector<VkDescriptorPoolSize> poolSize, int maxSets, VkDescriptorPool &descPool)
	{
		//set up the pool create info
		VkDescriptorPoolCreateInfo poolInfo;
		poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		poolInfo.poolSizeCount = poolSize.size();//size fo the vector
		poolInfo.pPoolSizes = poolSize.data();//vector data
		poolInfo.maxSets = maxSets;//numbert of sets to use
		poolInfo.flags = 0;

		if (vkCreateDescriptorPool(device, &poolInfo, nullptr, &descPool) != VK_SUCCESS)
		{
			return false; //return false
		}
		//return true
		return true;
	}

	//creates the descriptor sets for the uniform
	//takes in a logic device, descriptor pool, layouts, descriptor count
	//returns a VkDescriptorSet and a bool
	//returns false if failed
	bool createDescriptorSet(VkDevice device, VkDescriptorPool descPool, vector<VkDescriptorSetLayout> layouts, int descSetCount, VkDescriptorSet &descriptorSet)
	{
		VkDescriptorSetAllocateInfo allocInfo; //structure to allocate the set
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO; //structure type
		allocInfo.descriptorPool = descPool; //descriptor pool
		allocInfo.descriptorSetCount = descSetCount; //set count
		allocInfo.pSetLayouts = layouts.data();//layout data
		
		//allocate the descriptor set
		if (vkAllocateDescriptorSets(device, &allocInfo, &descriptorSet) != VK_SUCCESS) {
			return false;//if failed return false
		}

		return false;
	}

	//updates the uniform descriptor sets
	//takes a valid logic device, buffer info and descriptor set
	//int binding is the binding location of the uniform  
	//int arraryElememt array element
	//int descriptorCount number of descriptors
	void updateUniformDescriptorSets(VkDevice device, VkDescriptorBufferInfo &bufferInfo, VkDescriptorSet descriptorSet, int binding, int arraryElememt, int descriptorCount)
	{
		VkWriteDescriptorSet descriptorWrite;
		descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET; //structure type
		descriptorWrite.dstSet = descriptorSet; //descriptor set
		descriptorWrite.dstBinding = binding; //destination binding 
		descriptorWrite.dstArrayElement = arraryElememt;//arrayElement
		descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		descriptorWrite.descriptorCount = descriptorCount;//descriptor count
		descriptorWrite.pBufferInfo = &bufferInfo;


		//update the descriptor sets
		vkUpdateDescriptorSets(device, 1, &descriptorWrite, 0, nullptr);
	}

	void updateImageDescriptorSets(VkDevice device, VkImageView &imageView, VkSampler imageSampler, VkDescriptorSet descriptorSet, int binding, int arraryElememt, int descriptorCount)
	{
		VkDescriptorImageInfo imageInfo = {};
		imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imageInfo.imageView = imageView;
		imageInfo.sampler = imageSampler;

		VkWriteDescriptorSet descriptorWrite;
		descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET; //structure type
		descriptorWrite.dstSet = descriptorSet; //descriptor set
		descriptorWrite.dstBinding = binding; //destination binding 
		descriptorWrite.dstArrayElement = arraryElememt;//arrayElement
		descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		descriptorWrite.descriptorCount = descriptorCount;//descriptor count
		descriptorWrite.pImageInfo = &imageInfo;

		vkUpdateDescriptorSets(device, 1, &descriptorWrite, 0, nullptr);
	}

	VkDescriptorSetLayoutBinding createDescriptorSetLayoutBinding(int binding, VkDescriptorType type, VkShaderStageFlagBits stageFlags) {
		//create the create info
		VkDescriptorSetLayoutBinding uniformLayoutBinding;
		uniformLayoutBinding.binding = binding; //binding location
		uniformLayoutBinding.descriptorType = type;
		uniformLayoutBinding.descriptorCount = 1;//descriptor count
		uniformLayoutBinding.stageFlags = stageFlags;//stage flags
		uniformLayoutBinding.pImmutableSamplers = VK_NULL_HANDLE; // Optional
																  //return the binding
		return uniformLayoutBinding;
	}

	bool createDescriptorSetLayout(VkDevice device, VkDescriptorSetLayout &layout, vector<VkDescriptorSetLayoutBinding> layoutBindings) {
		VkDescriptorSetLayoutCreateInfo layoutInfo;//create the layout info
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;//structure type
		layoutInfo.bindingCount = layoutBindings.size();//number of layouts
		layoutInfo.pBindings = layoutBindings.data();//binding data
		layoutInfo.flags = 0;
		layoutInfo.pNext = VK_NULL_HANDLE;

		//create the descriptor layout return false if failed to create
		if (vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &layout) != VK_SUCCESS) {
			cout << "could not create the descriptor set layout\n";
			return false;
		}

		return true;
	}

	//this function uses the struct VKLib::graphicFamilyindex. This function fetches the queues supported by the device
	//and performs a check on each queue to see if it meets the requirement and passes the number of compatable structs to the struct
	graphicFamilyindex getgraphicFamilies(VkPhysicalDevice device, VkSurfaceKHR surface) {
		//queue count is the number of queues the device has
		uint32_t queueCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueCount, nullptr);

		//create a vector and fill it with the device queues
		std::vector<VkQueueFamilyProperties> graphicFamilies(queueCount);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueCount, graphicFamilies.data());

		//create a handle to the struct
		VKLib::graphicFamilyindex index;

		//for each queue loop through it and set the index struct the number of supported queues
		int i = 0;
		//use the iterator
		for (const auto& graphicFamily : graphicFamilies)
		{
			//ensure it points to a queue and check to see if the current queue  supports the required flag  
			if (graphicFamily.queueCount > 0 && graphicFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				index.graphicFam = i;
			}

			//get present support of the current devcie
			VkBool32 presentSupport = false;
			vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);

			if (graphicFamily.queueCount > 0 && presentSupport) {
				index.presentFam = i;
			}

			//break if its found enough
			if (index.isSuitable()) {
				break;
			}
			//increase i
			i++;
		}
		//return index
		return index;
	}

	SwapChainSupport querySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface) {
		//create a handle to get the swap chain support
		VKLib::SwapChainSupport details;

		//get the device surface capabilities
		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.surfaceCapabilities);

		//get the surface formats
		uint32_t formatCount;
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);

		if (formatCount != 0) {
			details.surfaceFormats.resize(formatCount);
			vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.surfaceFormats.data());
		}

		//same for the presentMode
		uint32_t presentModeCount;
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

		if (presentModeCount != 0) {
			details.presentModes.resize(presentModeCount);
			vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.presentModes.data());
		}

		return details;
	}
}